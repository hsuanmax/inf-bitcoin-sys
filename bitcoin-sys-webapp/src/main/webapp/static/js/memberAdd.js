


$(document).on('nifty.ready', function() {
    var faIcon = {
        valid: 'fa fa-check-circle fa-lg text-success',
        invalid: 'fa fa-times-circle fa-lg',
        validating: 'fa fa-refresh'
    }


    $('#addForm').bootstrapValidator({
        message: 'This value is not valid',
        excluded: ':disabled',
        feedbackIcons: faIcon,
        fields: {
            memberId: {
                validators: {
                    notEmpty: {
                        message: '此欄位不得為空'
                    },
                    regexp: {
                        regexp: /^[A-Z_0-9]+$/i,
                        message: '此欄位只可輸入英文、數字且不得有空格及任何符號'
                    }
                }
            },
            chName: {
                validators: {
                    notEmpty: {
                        message: '此欄位不得為空'
                    },
                    regexp: {
                        regexp: /^[\u4e00-\u9fa5\s]+$/,
                        message: '此欄位只可輸入中文'
                    }
                }
            },
            enName: {
                validators: {
                    notEmpty: {
                        message: '此欄位不得為空'
                    },
                    regexp: {
                        regexp: /^[A-Z\s]+$/i,
                        message: '此欄位只可輸入英文'
                    }
                }
            },
            referrerId: {
                validators: {
                    notEmpty: {
                        message: '此欄位不得為空'
                    },
                    regexp: {
                        regexp: /^[A-Z_0-9]+$/i,
                        message: '此欄位只可輸入英文、數字且不得有空格及任何符號'
                    },
                    remote: {
                        message: '無效的推薦人ID',
                        method: 'GET',
                        url: '/validReferrerId',
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: '此欄位不得為空'
                    },
                    emailAddress: {
                        message: '格式不符'
                    }
                }
            }
        }
    }).on('status.field.bv', function(e, data) {
        var $form = $(e.target),
            validator = data.bv,
            $collapsePane = data.element.parents('.collapse'),
            colId = $collapsePane.attr('id');

        if (colId) {
            var $anchor = $('a[href="#' + colId + '"][data-toggle="collapse"]');
            var $icon = $anchor.find('i');

            // Add custom class to panel containing the field
            if (data.status == validator.STATUS_INVALID) {
                $anchor.addClass('bv-col-error');
                $icon.removeClass(faIcon.valid).addClass(faIcon.invalid);
            } else if (data.status == validator.STATUS_VALID) {
                var isValidCol = validator.isValidContainer($collapsePane);
                if (isValidCol) {
                    $anchor.removeClass('bv-col-error');
                }else{
                    $anchor.addClass('bv-col-error');
                }
                $icon.removeClass(faIcon.valid + " " + faIcon.invalid).addClass(isValidCol ? faIcon.valid : faIcon.invalid);
            }
        }
    });


    $("#referrerId").blur(function(){
        var $groupDiv = $("#referrerId").parent().parent();
        var classValue = $groupDiv.attr("class");
        var hasSuccess = inputElementHasSuccess(classValue);
        // $("#memberCategory").val() == "" &&
        if(hasSuccess){
            getMemberCategeryInfo();
            return;
        }
        var hasError = inputElementHasError(classValue);
        if(hasError) $("#memberCategory").val("");
        
        var i;
    });

    function inputElementHasSuccess(classValue){
        var classes = classValue.split(" ");
        var result = false;
        $.each( classes, function( key, value ) {
            if(value == 'has-success') result = true;
        });
        return result;
    }
    function inputElementHasError(classValue){
        var classes = classValue.split(" ");
        var result = false;
        $.each( classes, function( key, value ) {
            if(value == 'has-error') result = true;
        });
        return result;
    }

    function getMemberCategeryInfo(){
        $.ajax({
            url: "/getCategoryInfo",
            method: "GET",
            data: { referrerId : $("#referrerId").val() },
            dataType: "json"
        }).done(function( result ) {
            $("#memberCategory").val(result.name);
            $("#hierarchyId").val(result.id);
            console.log( 'id:'+result.id);
        }).fail(function( jqXHR, textStatus ) {
            alert( "Request failed: " + textStatus );
        });
    }
});



