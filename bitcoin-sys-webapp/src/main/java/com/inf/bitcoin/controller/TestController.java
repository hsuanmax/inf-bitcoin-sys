package com.inf.bitcoin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TestController {
    
    @RequestMapping("myIncome")
    public String list(Model model){
        model.addAttribute("userName","Rick");
        model.addAttribute("userId","shao yu");
        return "incomeInfo";
    }

//    @RequestMapping("orderList")
//    public String orderList(Model model){
//        model.addAttribute("userName","Rick");
//        model.addAttribute("userId","shao yu");
//        return "orderList";
//    }

//    @RequestMapping("userInfo")
//    public String userInfo(Model model){
//        model.addAttribute("userName","Rick");
//        model.addAttribute("userId","shao yu");
//        return "userInfo";
//    }

//    @RequestMapping("list")
//    public String list(Model model){
//
//        model.addAttribute("userName","Rick");
//        model.addAttribute("userId","shao yu");
//        return "components/list";
//    }
//
//    @RequestMapping("edit")
//    public String edit(Model model){
//
//        model.addAttribute("userName","Rick");
//        model.addAttribute("userId","shao yu");
//        return "components/edit";
//    }
//
//    @RequestMapping("add")
//    public String add(Model model){
//
//        model.addAttribute("userName","Rick");
//        model.addAttribute("userId","shao yu");
//        return "components/add";
//    }

}
