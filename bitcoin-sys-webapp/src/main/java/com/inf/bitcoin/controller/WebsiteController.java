package com.inf.bitcoin.controller;

import com.inf.bitcoin.service.CoinmarketcapService;
import com.inf.bitcoin.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WebsiteController {

    @Autowired
    private CoinmarketcapService coinmarketcapService;

    @Autowired
    private NewsService newsService;

    @RequestMapping(value = {"","index"})
    public String index(Model model){

        model.addAttribute("newsList", newsService.listNewsByIndexPage(6));
        model.addAttribute("currencyList",coinmarketcapService.getTopTenCurrency(coinmarketcapService.getListings()));

        return "front/index";
    }

    @GetMapping("/aboutUs")
    public String aboutUs(Model model) {

        return "front/aboutUs";
    }

    @GetMapping("/contactUs")
    public String contactUs(Model model) {

        return "front/contactUs";
    }

    @GetMapping("/service")
    public String service(Model model) {

        return "front/services";
    }
}
