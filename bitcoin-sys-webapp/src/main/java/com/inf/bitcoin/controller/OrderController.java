package com.inf.bitcoin.controller;

import com.inf.bitcoin.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.SessionAttribute;

/**
 * Created by max on 2018/9/17.
 */
@Controller
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping("/memberOrderList")
    public String orderList(Model model, @SessionAttribute String memberId) {
        model.addAttribute("orderListForOwner", orderService.orderListForOwner(memberId));

        return "orderList";
    }
}
