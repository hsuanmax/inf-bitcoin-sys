package com.inf.bitcoin.controller;

import com.inf.bitcoin.model.DailyUnpaidProfit;
import com.inf.bitcoin.model.PaidIncome;
import com.inf.bitcoin.service.IncomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttribute;

import java.util.List;

@Controller
public class MemberIncomeController {

    @Autowired
    private IncomeService incomeService;

    @RequestMapping("/memberIncomeList")
    public String memberIncomeList(@SessionAttribute String memberId, Model model){

        model.addAttribute("incomeList", incomeService.findIncomeTempById(memberId));

        return "incomeHistory";

    }

    @RequestMapping("/memberIncomeInfo")
    public String memberIncomeInfo(@SessionAttribute String memberId, Model model){
        model.addAttribute("incomeInfo", incomeService.getMemberIncomInfo(memberId));
        return "incomeInfo";
    }

    @RequestMapping("/memberPaidIncome")
    public String memberPaidIncome(@SessionAttribute String memberId, Model model){
        List<PaidIncome> paidIncomeList = incomeService.getPaidIncomeList(memberId);
        model.addAttribute("incomeList", paidIncomeList);
        model.addAttribute("paidBtc", incomeService.getPaidBtc(paidIncomeList));
        return "paidIncome";
    }

    @RequestMapping("/memberHistoryIncome")
    public String memberHistoryIncome(@SessionAttribute String memberId, Model model){
        List<DailyUnpaidProfit> dailyUnpaidProfitList = incomeService.getDailyUnpaidProfitList(memberId);
        model.addAttribute("incomeList", dailyUnpaidProfitList);
        model.addAttribute("historyBtc", incomeService.getHistoryBtc(dailyUnpaidProfitList));
        return "incomeHistory";
    }

}
