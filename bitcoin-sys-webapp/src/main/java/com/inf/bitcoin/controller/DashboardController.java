package com.inf.bitcoin.controller;

import com.inf.bitcoin.model.Member;
import com.inf.bitcoin.service.DashboardService;
import com.inf.bitcoin.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttribute;

import java.math.BigDecimal;

/**
 * Created by max on 2018/9/18.
 */
@Controller
public class DashboardController {

    @Autowired
    private MemberService memberService;

    @Autowired
    private DashboardService dashboardService;

    @RequestMapping("/memberDashboard")
    public String dashboard(Model model, @SessionAttribute String memberId) {
        Member owner = memberService.getMemberById(memberId);
        BigDecimal totalBitcoin = dashboardService.calculateBitcoinByOwner(owner);
        model.addAttribute("totalBitcoin", totalBitcoin);

        return "dashboard";
    }
}
