package com.inf.bitcoin.controller;

import com.inf.bitcoin.model.Hierarchy;
import com.inf.bitcoin.model.Member;
import com.inf.bitcoin.service.MemberService;
import com.inf.bitcoin.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by max on 2018/9/21.
 */
@Controller
public class MemberManagementController {

    @Autowired
    private MemberService memberService;

    @Autowired
    private OrderService orderService;

    @GetMapping("/memberManagement")
    public String memberManagement(Model model, @SessionAttribute String memberId) {
        List<Member> memberList = memberService.getMemberListByHierarchy(memberId);
        model.addAttribute("memberList", memberList);

        return "components/list";
    }

    @GetMapping("/memberAdd")
    public String memberAdd(Model model) {
        model.addAttribute("member", new Member());

        return "components/add";
    }

    @PostMapping("/memberAddSubmit")
    public String memberAddSubmit(@ModelAttribute Member memeber,@SessionAttribute String memberId) {
        memberService.saveNewGeneralMember(memeber,memberId);

        return "redirect:/memberManagement";
    }

    @GetMapping("/memberEdit")
    public String memberEdit(Model model, @RequestParam String mid) {
        Member member = memberService.getMemberById(mid);
        model.addAttribute("member", member);
        model.addAttribute("orderListForOwner", orderService.orderListForOwner(mid));
        List<Hierarchy> hierarchyList = memberService.getMemeberHierarchy(mid);
        String sb = hierarchyList.stream().map(Hierarchy::getName).collect(Collectors.joining("、"));


        model.addAttribute("hierarchy",sb);


        return "components/edit";
    }

    @PostMapping("/memberEditSubmit")
    public String memberEditSubmit( @ModelAttribute Member member,@SessionAttribute String memberId) {
        memberService.modifyUserInfo(member.getMid(), member.getName(), member.getEnglishName(), member.getEmail(),memberId);

        return "redirect:/memberManagement";
    }

    @PostMapping("/memberDel")
    public String memberDelete(@RequestParam String mid) {
        memberService.delMember(mid);

        return "redirect:/memberManagement";
    }

    /**
     * 檢查推薦人是否存在
     * @param referrerId
     * @return
     */
    @RequestMapping("validReferrerId")
    @ResponseBody
    public Map<String,Object> referrerIdValid(@RequestParam String referrerId){
        Map<String,Object> result = new HashMap<>();
        // 用js的套件驗證，所以只能回傳valid是true/false; by Rick
        Member member = memberService.getMemberById(referrerId);
        result.put("valid",false);
        if(member != null) {
            result.put("valid",true);
        }
//        if("max666".equals(referrerId)){
//            result.put("valid",true);
//        }
        return result;
    }

    /**
     * 查詢推薦人的體系
     * @param referrerId
     * @return
     */
    @RequestMapping("getCategoryInfo")
    @ResponseBody
    public Map<String,Object> getMemeberInfo(@RequestParam String referrerId){
        Map<String,Object> result = new HashMap<>();
        result.put("name","");
        result.put("id","");
        Member member = memberService.getMemberById(referrerId);
        if(member != null){
            // js會發ajax上來，目前js是取name跟id
            // 如有其他需求 請改memberAdd.js的 getMemberCategeryInfo()內容

            List<Hierarchy> hierarchyList = memberService.getMemeberHierarchy(referrerId);

            if(CollectionUtils.isEmpty(hierarchyList) || hierarchyList.size()>1) return result;
            Hierarchy hierarchy = hierarchyList.get(0);
            result.put("name",hierarchy.getName());
            result.put("id",hierarchy.getHid());
        }
        return result;
    }
}
