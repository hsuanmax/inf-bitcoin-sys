package com.inf.bitcoin.controller;

import com.inf.bitcoin.component.CaptchaSettings;
import com.inf.bitcoin.model.Member;
import com.inf.bitcoin.service.MailService;
import com.inf.bitcoin.service.MemberService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.TemplateEngine;

/**
 * Created by max on 2018/9/13.
 */
@Controller
public class LoginController {

    @Autowired
    private MemberService memberService;

    @Autowired
    private MailService mailService;

    @Autowired
    private TemplateEngine templateEngine;

    @GetMapping("/login")
    public String login(Model model) {

        return "front/login";
    }

    @PostMapping("/loginError")
    public String loginError(Model model, @RequestAttribute String errorMsg) {
        model.addAttribute("loginError", true);
        model.addAttribute("errorMsg", errorMsg);

        return "front/login";
    }

    @RequestMapping("/memberResetPwd")
    public String resetPwd(Model model) {
        model.addAttribute("member", new Member());
        return "resetPwd";
    }

    @PostMapping("/memberResetPwdSubmit")
    public String resetPwdSubmit(@ModelAttribute Member member, @SessionAttribute String memberId) {
        member.setMid(memberId);
        memberService.resetPwdByFirstLogin(member);

        return "redirect:/memberDashboard";
    }

    @GetMapping("/forgetPwd")
    public String forgetPwd(Model model){

        return "forgetPwd";
    }

    @PostMapping("/forgetPwdSubmit")
    public String resetPwdSubmit(Model model, @RequestParam String mid) {
        Member member = memberService.getMemberById(mid);
        String msg = "";
        if(member==null) {
            msg = "輸入登入Id有誤，請確認";
        }else{
            if(mailService.sendForgetPwdMail(member, "mail/forgetPwdTemp", templateEngine)){
                msg = "系統已發送新密碼至註冊信箱，請確認";
            }else {
                msg = "系統發生錯誤，請聯絡系統管理員";
            }
        }
        model.addAttribute("msg", msg);

        return "forgetPwd";
    }
}
