package com.inf.bitcoin.controller;

import com.inf.bitcoin.model.News;
import com.inf.bitcoin.service.CategoryService;
import com.inf.bitcoin.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

/**
 * Created by max on 2018/10/22.
 */
@Controller
public class NewsController {

    @Autowired
    private NewsService newsService;

    @Autowired
    private CategoryService categoryService;

    @GetMapping(value = "/image/{imageName:.+}")
    @ResponseBody
    public byte[] getImage(@PathVariable String imageName) throws IOException {

        return newsService.getImage(imageName);
    }

    @GetMapping("/newsView/{nid}")
    public String newsView(@PathVariable Long nid, Model model) {
        News news = newsService.getNewsForEditoOrShow(nid);
        model.addAttribute("news", news);
        model.addAttribute("categoryList", categoryService.listCategory());

        return "front/newsView";
    }

    @GetMapping("/newsListCategory")
    public String newsListCategory(Model model) {
        model.addAttribute("newsList", newsService.listNewsByIndexPage(6));
        model.addAttribute("categoryList", categoryService.listCategory());

        return "front/newsListCategory";
    }

    @GetMapping("/newsListCategory/{cid}")
    public String newsListCategory(@PathVariable Long cid, Model model) {
        model.addAttribute("newsList", newsService.listNewsByCategory(cid));
        model.addAttribute("categoryList", categoryService.listCategory());

        return "front/newsListCategory";
    }
}
