package com.inf.bitcoin.controller;

import com.inf.bitcoin.model.Hierarchy;
import com.inf.bitcoin.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
public class MemberInfoController {
    @Autowired
    private MemberService memberService;

    @RequestMapping("/memberInfo")
    public String memberInfo(Model model, @SessionAttribute String memberId){
        model.addAttribute("memberInfo", memberService.getMemberById(memberId));
        List<Hierarchy> hierarchyList = memberService.getMemeberHierarchy(memberId);
        String sb = hierarchyList.stream().map(Hierarchy::getName).collect(Collectors.joining("、"));

        model.addAttribute("hierarchy",sb);
        return "memberInfo";
    }

    @RequestMapping(value = "/memberModifyInfo",method= RequestMethod.POST)
    public String modifyUserInfo(@SessionAttribute String memberId,
                                 @RequestParam String userChName,
                                 @RequestParam String userEnName,
                                 @RequestParam String email){

        memberService.modifyUserInfo(memberId,userChName,userEnName,email,memberId);
        
        return  "forward:/memberInfo";
    }

    @RequestMapping(value = "/memberModifyWallet",method= RequestMethod.POST)
    public String modifyWallet(@SessionAttribute String memberId,
                               @RequestParam String walletCode,
                               @RequestParam String minimumPayout){
        memberService.modifyWallet(memberId,walletCode,minimumPayout);
        return  "forward:/memberInfo";
    }

    @RequestMapping(value = "/memberModifyPw",method= RequestMethod.POST)
    public String modifyPw(@SessionAttribute String memberId,
                               Model model,
                               @RequestParam String oldPw,
                               @RequestParam String newPw,
                               @RequestParam String checkNewPw){

        if( !newPw.equals(checkNewPw)){
            model.addAttribute("msg","新密碼與確認密碼不同");
            model.addAttribute("memberInfo", memberService.getMemberById(memberId));
            return "memberInfo";
        }
        Map<String,String> result = memberService.modifyPw(memberId,oldPw,newPw);
        if("".equals(result.get("errorMsg"))){
            model.addAttribute("msg","變更密碼成功");
        }else{
            model.addAttribute("msg",result.get("errorMsg"));
        }
        model.addAttribute("memberInfo", memberService.getMemberById(memberId));
        return  "memberInfo";
    }

}
