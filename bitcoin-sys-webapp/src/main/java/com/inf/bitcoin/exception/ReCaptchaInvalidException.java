package com.inf.bitcoin.exception;

import org.springframework.security.core.AuthenticationException;

public final class ReCaptchaInvalidException extends AuthenticationException {

    private static final long serialVersionUID = 5861310537366287163L;


    public ReCaptchaInvalidException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public ReCaptchaInvalidException(final String message) {
        super(message);
    }


}
