package com.inf.bitcoin.security;

import com.inf.bitcoin.exception.ReCaptchaInvalidException;
import com.inf.bitcoin.service.CaptchaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by max on 2018/9/18.
 */
public class BitcoinSysLoginFilter extends AbstractAuthenticationProcessingFilter {

    @Autowired
    private BitcoinSysLoginSuccessHandler bitcoinSysLoginSuccessHandler;

    @Autowired
    private BitcoinSysLoginFailureHandler bitcoinSysLoginFailureHandler;

    @Autowired
    private CaptchaService captchaService;

    public BitcoinSysLoginFilter(String url, AuthenticationManager authManager) {
        super(new AntPathRequestMatcher(url, HttpMethod.POST.toString()));
        setAuthenticationManager(authManager);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {

        try {
//            captchaService.processResponse(request.getParameter("g-recaptcha-response"), request.getRemoteAddr());
            captchaService.processResponse(request.getParameter("luotest_response"), request.getRemoteAddr());
        } catch (Exception e) {
            throw new ReCaptchaInvalidException("請完成『我不是機器人』驗證");
        }

        String ipAddress = this.getClientIP(request);

        String userId = request.getParameter("username");
        String password = request.getParameter("password");
        BitcoinSysMemberToken bitcoinSysMemberToken =
                new BitcoinSysMemberToken(userId, password, ipAddress);

        return this.getAuthenticationManager().authenticate(bitcoinSysMemberToken);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        SecurityContextHolder.getContext().setAuthentication(authResult);
        bitcoinSysLoginSuccessHandler.onAuthenticationSuccess(request, response, authResult);
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
        bitcoinSysLoginFailureHandler.onAuthenticationFailure(request, response, failed);
    }

    private String getClientIP(HttpServletRequest request) {
        String xfHeader = request.getHeader("X-Forwarded-For");
        if (xfHeader == null){
            return request.getRemoteAddr();
        }
        return xfHeader.split(",")[0];
    }
}
