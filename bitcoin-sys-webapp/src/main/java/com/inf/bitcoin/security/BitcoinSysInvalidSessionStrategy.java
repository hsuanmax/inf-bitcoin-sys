package com.inf.bitcoin.security;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.session.InvalidSessionStrategy;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class BitcoinSysInvalidSessionStrategy implements InvalidSessionStrategy {
    private final RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
    private boolean createNewSession = true;

    public void onInvalidSessionDetected(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (this.createNewSession) {
            request.getSession();
        }

        String param = (String)request.getParameter("lang");
        String redirectUrl = "/login";
        if(StringUtils.isNotBlank(param)){
            redirectUrl = redirectUrl+"?lang="+param;
        }



        this.redirectStrategy.sendRedirect(request, response, redirectUrl);
    }


}
