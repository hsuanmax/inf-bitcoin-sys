package com.inf.bitcoin.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by max on 2018/4/12.
 */
@Component
public class BitcoinSysLoginSuccessHandler implements AuthenticationSuccessHandler {

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    public void onAuthenticationSuccess(
            HttpServletRequest request, HttpServletResponse response,
            Authentication authentication) throws IOException, ServletException {

        handle(request, response, authentication);
        clearAuthenticationAttributes(request);
    }

    protected void handle(HttpServletRequest request,
                          HttpServletResponse response, Authentication authentication)
            throws IOException {
        BitcoinSysMemberDetails bitcoinSysMemberDetails = (BitcoinSysMemberDetails)authentication.getPrincipal();
        HttpSession httpSession = request.getSession();
        httpSession.setAttribute("memberId", bitcoinSysMemberDetails.getUsername());
        httpSession.setAttribute("memberName", bitcoinSysMemberDetails.getMemberName());
        List<GrantedAuthority> authorityList = new ArrayList<GrantedAuthority>(bitcoinSysMemberDetails.getAuthorities());
        boolean isLeader = false;
        for(GrantedAuthority auth:authorityList){
            if(auth.getAuthority().equals("LEADER"))isLeader = true;
        }
        httpSession.setAttribute("isLeader", isLeader);

        String targetUrl = determineTargetUrl(bitcoinSysMemberDetails);


        redirectStrategy.sendRedirect(request, response, targetUrl);
    }

    protected String determineTargetUrl(BitcoinSysMemberDetails bitcoinSysMemberDetails) {
        boolean isMember = false;
        Collection<? extends GrantedAuthority> authorities
                = bitcoinSysMemberDetails.getAuthorities();
        for (GrantedAuthority grantedAuthority : authorities) {
            if (grantedAuthority.getAuthority().equals("LEADER") || grantedAuthority.getAuthority().equals("GENERAL")) {
                isMember = true;
                break;
            }
        }

        if(!bitcoinSysMemberDetails.getHasChangePwd()) {
            return "/memberResetPwd";
        }

        if (isMember) {
            return "/memberDashboard";
        } else {
            throw new IllegalStateException();
        }
    }

    protected void clearAuthenticationAttributes(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return;
        }
        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
    }

    public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
        this.redirectStrategy = redirectStrategy;
    }
    protected RedirectStrategy getRedirectStrategy() {
        return redirectStrategy;
    }
}
