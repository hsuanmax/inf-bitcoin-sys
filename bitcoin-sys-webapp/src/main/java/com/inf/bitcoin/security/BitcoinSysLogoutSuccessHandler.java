package com.inf.bitcoin.security;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class BitcoinSysLogoutSuccessHandler implements LogoutSuccessHandler {


    @Override
    public void onLogoutSuccess(HttpServletRequest request,
                                HttpServletResponse response, Authentication authentication)
            throws IOException {
        SessionLocaleResolver r = new SessionLocaleResolver();
        
        String URL = request.getContextPath() + "/login?lang="+r.resolveLocale(request).toString();
        request.getSession().invalidate();
        response.setStatus(HttpStatus.OK.value());
        response.sendRedirect(URL);
    }

}
