package com.inf.bitcoin.security;

import com.inf.bitcoin.model.BlackList;
import com.inf.bitcoin.model.Member;
import com.inf.bitcoin.model.Role;
import com.inf.bitcoin.service.BlackListService;
import com.inf.bitcoin.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by max on 2018/10/18.
 */
@Component
public class BitcoinSysAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    @Autowired
    private MemberService memberService;

    @Autowired
    private BlackListService blackListService;

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {
        BitcoinSysMemberToken bitcoinSysMemberToken = (BitcoinSysMemberToken)usernamePasswordAuthenticationToken;
        String pwd = userDetails.getPassword();
        List<GrantedAuthority> authorities = new ArrayList(userDetails.getAuthorities());

        if(!new BCryptPasswordEncoder().matches(
                bitcoinSysMemberToken.getCredentials().toString(), pwd)) {
            blackListService.processLoginFail(bitcoinSysMemberToken.getIpAddress());
            throw new BadCredentialsException("登入失敗，密碼不正確");
        }
        if(authorities.size()==0) {
            throw new BadCredentialsException("尚未設定權限，請洽系統管理員");
        }
    }

    @Override
    protected UserDetails retrieveUser(String s, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {
        BitcoinSysMemberToken bitcoinSysMemberToken = (BitcoinSysMemberToken)usernamePasswordAuthenticationToken;
        String ipAddress = bitcoinSysMemberToken.getIpAddress();
        List<BlackList> blackList = blackListService.list(ipAddress);
        if(!blackList.isEmpty())
            throw new BadCredentialsException("你的IP已被鎖定，請聯絡系統管理員");

        Member member = memberService.getMemberById(bitcoinSysMemberToken.getName());
        if(member==null) {
            blackListService.processLoginFail(bitcoinSysMemberToken.getIpAddress());
            throw new BadCredentialsException("登入失敗，帳號不正確");
        }

        BitcoinSysMemberDetails bitcoinSysMemberDetails =
                new BitcoinSysMemberDetails(member.getMid(), member.getPassword(), this.getAuthorities(member.getRoles()));
        bitcoinSysMemberDetails.setMemberName(member.getName());
        bitcoinSysMemberDetails.setHasChangePwd(member.getHasChangePwd());

        return bitcoinSysMemberDetails;
    }

    private List<GrantedAuthority> getAuthorities(Set<Role> roles) {
        List<GrantedAuthority> authorityList = new ArrayList<>();
        for(Role role:roles) {
            SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(role.getRid());
            authorityList.add(simpleGrantedAuthority);
        }

        return authorityList;
    }

}
