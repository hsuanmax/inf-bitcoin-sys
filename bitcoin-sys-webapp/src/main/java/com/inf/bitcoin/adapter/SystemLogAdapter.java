package com.inf.bitcoin.adapter;

import com.inf.bitcoin.model.LogType;
import com.inf.bitcoin.model.Member;
import com.inf.bitcoin.model.SystemLog;
import com.inf.bitcoin.service.MemberService;
import com.inf.bitcoin.service.SystemLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * Created by max on 2018/11/13.
 */
public class SystemLogAdapter extends HandlerInterceptorAdapter {

    @Autowired
    private SystemLogService systemLogService;

    @Autowired
    private MemberService memberService;

    private Member member;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String memberId = (String)request.getSession().getAttribute("memberId");
        member = memberService.getMemberById(memberId);

        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response,
                                Object handler, @Nullable Exception ex) throws Exception {

        String walletCode = request.getParameter("walletCode");
        String minimumPayout = request.getParameter("minimumPayout");
        String email = request.getParameter("email");

        if(!StringUtils.isEmpty(walletCode) && !member.getWalletCode().equals(walletCode)) {
            SystemLog systemLog = new SystemLog();
            systemLog.setMemberId(member.getMid());
            systemLog.setLogDesc(LogType.CHANGE_WALLET);
            systemLog.setOldValue(member.getWalletCode());
            systemLog.setNewValue(walletCode);
            systemLog.setDate(new Date());

            systemLogService.saveSystemLog(systemLog);
        }

        if(!StringUtils.isEmpty(minimumPayout) && !member.getMinimumPayout().equals(Float.valueOf(minimumPayout))) {
            SystemLog systemLog = new SystemLog();
            systemLog.setMemberId(member.getMid());
            systemLog.setLogDesc(LogType.CHANGE_MIN_PAYOUT);
            systemLog.setOldValue(member.getMinimumPayout().toString());
            systemLog.setNewValue(minimumPayout);
            systemLog.setDate(new Date());

            systemLogService.saveSystemLog(systemLog);
        }

        if(!StringUtils.isEmpty(email) && !member.getEmail().equals(email)) {
            SystemLog systemLog = new SystemLog();
            systemLog.setMemberId(member.getMid());
            systemLog.setLogDesc(LogType.CHANGE_EMAIL);
            systemLog.setOldValue(member.getEmail());
            systemLog.setNewValue(email);
            systemLog.setDate(new Date());

            systemLogService.saveSystemLog(systemLog);
        }

    }
}
