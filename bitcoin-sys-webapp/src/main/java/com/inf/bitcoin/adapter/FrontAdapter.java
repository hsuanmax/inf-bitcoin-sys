package com.inf.bitcoin.adapter;

import com.inf.bitcoin.model.News;
import com.inf.bitcoin.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class FrontAdapter extends HandlerInterceptorAdapter {
    @Autowired
    private NewsService newsService;


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession();
        List<News> newsList = (List<News>)session.getAttribute("newsOfFooter");
        if(CollectionUtils.isEmpty(newsList)){
            session.setAttribute("newsOfFooter",newsService.listNewsByIndexPage(2));
        }
        return true;
    }

  
    
}
