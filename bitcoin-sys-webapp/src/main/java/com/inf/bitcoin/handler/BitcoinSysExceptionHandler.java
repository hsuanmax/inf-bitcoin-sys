package com.inf.bitcoin.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.thymeleaf.exceptions.TemplateInputException;

/**
 * Created by max on 2018/2/26.
 */
@ControllerAdvice
public class BitcoinSysExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(BitcoinSysExceptionHandler.class);

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public String internalServerError(Exception ex){
        ex.printStackTrace();

        return "error/500";
    }

}
