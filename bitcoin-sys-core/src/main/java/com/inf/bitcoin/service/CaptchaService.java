package com.inf.bitcoin.service;

import com.inf.bitcoin.bean.json.GoogleResponse;
import com.inf.bitcoin.bean.json.LuosimaoResponse;
import com.inf.bitcoin.component.CaptchaSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.regex.Pattern;

@Service
public class CaptchaService {

    @Autowired
    private CaptchaSettings captchaSettings;

    private static Pattern RESPONSE_PATTERN = Pattern.compile("[A-Za-z0-9_-]+");

    public void processResponse(String response,String ip) throws Exception {
        if(!responseSanityCheck(response)) {
            throw new Exception("Response contains invalid characters");
        }

//        URI verifyUri = URI.create(String.format(
//                "https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s&remoteip=%s",
        URI verifyUri = URI.create(String.format(
                "https://captcha.luosimao.com/api/site_verify?api_key=%s&response=%s&remoteip=%s",
                captchaSettings.getApi(), response, ip));
        RestTemplate restTemplate = new RestTemplate();
//        GoogleResponse googleResponse = restTemplate.getForObject(verifyUri, GoogleResponse.class);
        LuosimaoResponse luosimaoResponse = restTemplate.getForObject(verifyUri, LuosimaoResponse.class);

//        if(!googleResponse.isSuccess()) {
        if(!luosimaoResponse.getError().equals("0")) {
            throw new Exception("reCaptcha was not successfully validated");
        }
    }

    private boolean responseSanityCheck(String response) {
        return StringUtils.hasLength(response) && RESPONSE_PATTERN.matcher(response).matches();
    }


}
