package com.inf.bitcoin.service;

import com.inf.bitcoin.model.SystemLog;
import com.inf.bitcoin.repository.SystemLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by max on 2018/11/13.
 */
@Service
public class SystemLogService {

    @Autowired
    private SystemLogRepository systemLogRepository;

    public void saveSystemLog(SystemLog systemLog) {
        systemLogRepository.save(systemLog);
    }
}
