package com.inf.bitcoin.service;

import com.inf.bitcoin.model.Member;
import com.inf.bitcoin.model.Role;
import com.inf.bitcoin.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class RoleService {
    @Autowired
    private RoleRepository roleRepository;

    public List<Member> getMemberListByLeader(){
        Role role = roleRepository.findById("LEADER").get();

        //移除大領導(多個體系)
        Map<String,Member> tempMap = new HashMap<>();
        for(Member member:role.getMembers()){
            if(member.getHierarchies().size()==1){
                tempMap.put(member.getMid(),member);
            }
        }

        return tempMap.values().stream().collect(Collectors.toList());
    }
}
