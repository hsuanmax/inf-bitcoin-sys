package com.inf.bitcoin.service;

import com.inf.bitcoin.bean.form.ReportIncomeForm;
import com.inf.bitcoin.bean.response.ReportIncome;
import com.inf.bitcoin.dao.ReportDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class ReportService {

    @Autowired
    private ReportDao reportDao;

    public List<ReportIncome> getReportIncomeList(ReportIncomeForm formBean){
        return convertBean(reportDao.getReport(formBean),ReportIncome.class);
    }

    protected <T> List<T> convertBean(List<Map<String,Object>> beans,Class<T> clazz){
        List<T> result = new ArrayList<>();
        try {
            for(Map<String,Object> row:beans){
                T item = clazz.newInstance();
                for(Field field:clazz.getDeclaredFields()){
                    String fieldName = field.getName();
                    if(row.containsKey(fieldName)){
                        String methodName = "set"+fieldName.substring(0,1).toUpperCase()+fieldName.substring(1);
                        Method method = clazz.getMethod(methodName,field.getType());
                        method.invoke(item,row.get(fieldName));
                    }
                }
                result.add(item);
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return result;
    }
}
