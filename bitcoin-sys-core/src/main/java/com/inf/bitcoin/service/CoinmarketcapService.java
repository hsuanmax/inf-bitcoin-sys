package com.inf.bitcoin.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.inf.bitcoin.bean.json.CpListingsLatest;
import com.inf.bitcoin.bean.json.CryptocurrencyPrices;
import com.inf.bitcoin.bean.response.CpCurrency;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CoinmarketcapService {
    private final String URL = "https://pro-api.coinmarketcap.com/v1/cryptocurrency/";
    private final String imgPath = "https://s2.coinmarketcap.com/static/img/coins/64x64/";

    public List<CryptocurrencyPrices> getListings(){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-CMC_PRO_API_KEY", "4e76e484-7202-4310-8c80-07bda2b449fa");
        
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);


        ResponseEntity<String> response = restTemplate.exchange(URL+"listings/latest", HttpMethod.GET, entity, String.class);
        ObjectMapper objectMapper = new ObjectMapper();

        CpListingsLatest cpListingsLatest;
        try {
            cpListingsLatest = objectMapper.readValue(response.getBody(), CpListingsLatest.class);
            if(cpListingsLatest.getStatus().getErrorCode().equals(0l)) return cpListingsLatest.getData();
        }catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }

    public List<CpCurrency> getTopTenCurrency(List<CryptocurrencyPrices> cryptocurrencyPricesGroup){
        if(CollectionUtils.isEmpty(cryptocurrencyPricesGroup)) return null;
        List<CryptocurrencyPrices> topTenCurrency = cryptocurrencyPricesGroup.stream().filter(data -> data.getCmcRank().intValue()<=10 ).collect(Collectors.toList());
        List<CpCurrency> result = new ArrayList<>();
        CpCurrency cpCurrency = null;
        NumberFormat numberFormat1 = NumberFormat.getNumberInstance();
        for(CryptocurrencyPrices cpBean : topTenCurrency){
            cpCurrency = new CpCurrency();
            cpCurrency.setImgPath(imgPath+String.valueOf(cpBean.getId())+".png");
            cpCurrency.setPercentChange24h(cpBean.getQuote().get("USD").getPercentChange24h().toString());
            cpCurrency.setPrice(numberFormat1.format(cpBean.getQuote().get("USD").getPrice().setScale(2, BigDecimal.ROUND_UP).doubleValue()));
            cpCurrency.setTotalSupply(numberFormat1.format(cpBean.getTotalSupply().longValue()));
            cpCurrency.setVolume24h(numberFormat1.format(cpBean.getQuote().get("USD").getVolume24h().longValue()));
            cpCurrency.setCurrencyName(cpBean.getName());
            cpCurrency.setSymbol(cpBean.getSymbol());
            result.add(cpCurrency);
        }
        return result;
    }
}
