package com.inf.bitcoin.service;

import com.inf.bitcoin.model.Hierarchy;
import com.inf.bitcoin.repository.HierarchyRepository;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class HierarchyService {

    private Logger logger = LoggerFactory.getLogger(HierarchyService.class);


    @Autowired
    private HierarchyRepository hierarchyRepository;

    @Autowired
    private MemberService memberService;

    public List<Hierarchy> findAll(){
        List<Hierarchy> result = new ArrayList<>();
        hierarchyRepository.findAll().forEach(result::add);
        return result;
    }

    public Boolean hasNotSameName(String name){
        List hierarchyList = hierarchyRepository.findByName(name.trim());
        return CollectionUtils.isEmpty(hierarchyList);
    }

    public void saveHierarchy(String name,String creatorId){
        logger.info("!!!!!!@@@@@@@"+name);
        Hierarchy hierarchy = new Hierarchy();
        hierarchy.setName(name);
        hierarchy.setCreator(memberService.getMemberById(creatorId));
        hierarchy.setCreatedDate(new Date());
        hierarchyRepository.save(hierarchy);
    }

    public Hierarchy findById(String hid){
        return hierarchyRepository.findById(Long.parseLong(hid)).get();
    }

    public void modifyHierarchy(String hid,String name,String editorId){
        Hierarchy hierarchy= hierarchyRepository.findById(Long.parseLong(hid)).get();
        hierarchy.setEditor(memberService.getMemberById(editorId));
        hierarchy.setUpdateDate(new Date());
        hierarchy.setName(name);
        hierarchyRepository.save(hierarchy);
    }

    public void deleteHierarchy(String hid){
        hierarchyRepository.deleteById(Long.parseLong(hid));
    }
}
