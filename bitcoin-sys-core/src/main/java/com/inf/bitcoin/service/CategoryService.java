package com.inf.bitcoin.service;

import com.inf.bitcoin.model.Category;
import com.inf.bitcoin.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by max on 2018/10/5.
 */
@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    public List<Category> listCategory() {
        List<Category> resultList = new ArrayList<Category>();
        categoryRepository.findAll().forEach(resultList::add);

        return resultList;
    }
}
