package com.inf.bitcoin.service;

import com.inf.bitcoin.model.Member;
import com.inf.bitcoin.model.Order;
import com.inf.bitcoin.repository.OrderRepository;
import com.inf.bitcoin.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Set;

/**
 * Created by max on 2018/9/18.
 */
@Service
public class DashboardService {

    @Autowired
    private MemberRepository memberRepository;

    @Autowired
    private OrderRepository orderRepository;

    public BigDecimal calculateBitcoinByOwner(Member member) {
        BigDecimal totalBitcoin = new BigDecimal(0);
        Set<Order> orders = member.getOrdersForOwner();
        for(Order order:orders) {
            totalBitcoin = totalBitcoin.add(order.getBoughtBitcoin()).add(order.getFreeBitcoin());
        }

        return totalBitcoin;
    }
}
