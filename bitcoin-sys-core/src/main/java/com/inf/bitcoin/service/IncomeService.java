package com.inf.bitcoin.service;

import com.inf.bitcoin.bean.response.MemberIncomInfo;
import com.inf.bitcoin.model.DailyUnpaidProfit;
import com.inf.bitcoin.model.IncomeTemp;
import com.inf.bitcoin.model.PaidIncome;
import com.inf.bitcoin.repository.DailyUnpaidProfitRepository;
import com.inf.bitcoin.repository.IncomeTempRepository;
import com.inf.bitcoin.repository.PaidIncomeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;

@Service
public class IncomeService {
    @Autowired
    private IncomeTempRepository incomeTempRepository;

    @Autowired
    private PaidIncomeRepository paidIncomeRepository;

    @Autowired
    private DailyUnpaidProfitRepository dailyUnpaidProfitRepository;

    public List<IncomeTemp> findIncomeTempById(String mid){
        return incomeTempRepository.findByMid(mid);
    }

    public MemberIncomInfo getMemberIncomInfo(String memberId){
        MemberIncomInfo result = new MemberIncomInfo();


        BigDecimal historyBtc = getHistoryBtc(getDailyUnpaidProfitList(memberId));
        BigDecimal paidBtc = getPaidBtc(getPaidIncomeList(memberId));
        BigDecimal unpaidBtc = historyBtc.subtract(paidBtc).setScale(8,BigDecimal.ROUND_HALF_UP) ;
        result.setHistoryBtc(historyBtc.toPlainString());
        result.setPaidBtc(paidBtc.toPlainString());
        result.setUnpaidBtc(unpaidBtc.toPlainString());
        return result;
    }

    public List<DailyUnpaidProfit> getDailyUnpaidProfitList(String memberId){
        return dailyUnpaidProfitRepository.findDailyUnpaidProfitsByMemberIdOrderByCreateDateDesc(memberId);
    }
    public List<PaidIncome> getPaidIncomeList(String memberId){
        return paidIncomeRepository.findPaidIncomesByMemberIdOrderByPayDateDesc(memberId);
    }

    public BigDecimal getHistoryBtc(List<DailyUnpaidProfit> dailyUnpaidProfitList){
        if(CollectionUtils.isEmpty(dailyUnpaidProfitList)) return new BigDecimal("0").setScale(8,BigDecimal.ROUND_HALF_UP);
        Double dailyUnpaidProfitSum = dailyUnpaidProfitList.stream()
                .mapToDouble(pojo -> pojo.getBoughtTotalT().add(pojo.getFreeTotalT()).doubleValue())
                .sum();
        return new BigDecimal(dailyUnpaidProfitSum).setScale(8,BigDecimal.ROUND_HALF_UP);
    }

    public BigDecimal getPaidBtc(List<PaidIncome> paidList){
        if(CollectionUtils.isEmpty(paidList)) return new BigDecimal("0").setScale(8,BigDecimal.ROUND_HALF_UP);
        Double paidSum = paidList.stream()
                .mapToDouble(pojo->pojo.getPaidCoin().doubleValue())
                .sum();
        return new BigDecimal(paidSum).setScale(8,BigDecimal.ROUND_HALF_UP);
    }
}
