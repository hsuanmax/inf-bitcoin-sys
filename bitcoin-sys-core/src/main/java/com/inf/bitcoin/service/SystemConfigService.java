package com.inf.bitcoin.service;

import com.inf.bitcoin.model.SystemConfig;
import com.inf.bitcoin.repository.SystemConfigRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SystemConfigService {

    @Autowired
    private SystemConfigRepository systemConfigRepository;

    public List<SystemConfig> findAll(){
        List<SystemConfig> result = new ArrayList<>();
        systemConfigRepository.findAll().forEach(result::add);
        return result;
    }

    public void modifySystemConfig(String paramId,String paramValue){
        
        SystemConfig systemConfig = systemConfigRepository.findById(paramId).get();
        systemConfig.setParamValue(paramValue.trim());
        systemConfigRepository.save(systemConfig);
    }

    public String findByParamId(String paramId) {
        String result = systemConfigRepository.findById(paramId).get().getParamValue();

        return result;
    }

}
