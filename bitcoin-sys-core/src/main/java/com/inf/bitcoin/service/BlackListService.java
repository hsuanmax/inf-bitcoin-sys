package com.inf.bitcoin.service;

import com.inf.bitcoin.model.BlackList;
import com.inf.bitcoin.model.LoginErrorRecord;
import com.inf.bitcoin.repository.BlackListRepository;
import com.inf.bitcoin.repository.LoginErrorRecordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by max on 2018/10/10.
 */
@Service
public class BlackListService {

    @Autowired
    private BlackListRepository blackListRepository;

    @Autowired
    private LoginErrorRecordRepository loginErrorRecordRepository;

    @Autowired
    private SystemConfigService systemConfigService;

    private static final String loginErrorCountStr = "loginErrorCount";

    public void saveLoginErrorRecord(LoginErrorRecord loginErrorRecord) {
        loginErrorRecordRepository.save(loginErrorRecord);
    }

    public List<BlackList> list(String ipAddress) {
        return blackListRepository.findByIpAddress(ipAddress);
    }

    public List<BlackList> listAll() {
        List<BlackList> resultList = new ArrayList<>();
        blackListRepository.findAll().forEach(resultList::add);

        return resultList;
    }

    public void save(BlackList blackList) {
        blackListRepository.save(blackList);
    }

    public void unLock(Long id) {
        BlackList blackList = blackListRepository.findById(id).get();
        String ipAddress = blackList.getIpAddress();
        List<LoginErrorRecord> loginErrorRecordList = loginErrorRecordRepository.findByIpAddress(ipAddress);
        blackListRepository.delete(blackList);
        loginErrorRecordRepository.deleteAll(loginErrorRecordList);
    }

    public int getLoginErrorCount(String ipAddress) {
        Calendar cal = Calendar.getInstance();
        Date endDate = cal.getTime();
        cal.add(Calendar.DAY_OF_MONTH, -1);
        Date startDate = cal.getTime();
        return loginErrorRecordRepository.countByIpAddressAndErrorDateBetween(ipAddress, startDate, endDate);
    }

    public void processLoginFail(String ipAddress) {
        Calendar cal = Calendar.getInstance();
        LoginErrorRecord loginErrorRecord = new LoginErrorRecord();
        loginErrorRecord.setIpAddress(ipAddress);
        loginErrorRecord.setErrorDate(cal.getTime());
        this.saveLoginErrorRecord(loginErrorRecord);
        int errorCount = this.getLoginErrorCount(ipAddress);
        String loginErrorCount = systemConfigService.findByParamId(loginErrorCountStr);
        if(errorCount >= Integer.parseInt(loginErrorCount)-1) {
            BlackList blackList = new BlackList();
            blackList.setIpAddress(ipAddress);
            blackList.setLockDate(new Date());
            this.save(blackList);
        }
    }

}
