package com.inf.bitcoin.service;

import com.inf.bitcoin.model.Member;
import com.inf.bitcoin.model.Order;
import com.inf.bitcoin.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by max on 2018/9/17.
 */
@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private MemberService memberService;

    public List<Order> orderListForOwner(String mid) {
        Member owner = new Member();
        owner.setMid(mid);
        List<Order> orderList = orderRepository.findByOwner(owner);
//        List<Order> resultList = new ArrayList<Order>();
        for(Order order:orderList) {
            BigDecimal totalBitcoin = new BigDecimal(0);
            totalBitcoin = totalBitcoin.add(order.getBoughtBitcoin()).add(order.getFreeBitcoin());
            order.setTotalBitcoin(totalBitcoin);
        }

        return orderList;
    }

    public Order getOrderById(String orderId){
        Order order = orderRepository.findById(Long.parseLong(orderId)).get();
        order.setTotalBitcoin(order.getBoughtBitcoin().add(order.getFreeBitcoin()));
        return order;
    }

    public void createOrder(Order newOrder,String ownerId,String creatorId){
        Member creator = memberService.getMemberById(creatorId);
        Member owner = memberService.getMemberById(ownerId);

        newOrder.setCreatedDate(new Date());
        newOrder.setCreator(creator);
        newOrder.setOwner(owner);
        orderRepository.save(newOrder);
    }

    public void modifyOrder(Order newOrder,String editorId){
        Member editor = memberService.getMemberById(editorId);
        Order order = orderRepository.findById(newOrder.getId()).get();
        newOrder.setUpdateDate(new Date());
        newOrder.setEditor(editor);
        newOrder.setOwner(order.getOwner());
        newOrder.setCreator(order.getCreator());
        newOrder.setCreatedDate(order.getCreatedDate());
        orderRepository.save(newOrder);
    }

    public void deleteOrder(String orderId){
        orderRepository.deleteById(Long.parseLong(orderId));
    }

    public BigDecimal getBoughtTotalT(Set<Order>orderSet) {
        BigDecimal result = new BigDecimal(0);
        for(Order order:orderSet) {
            result = result.add(order.getBoughtBitcoin());
        }

        return result;
    }

    public BigDecimal getFreeTotalT(Set<Order>orderSet) {
        BigDecimal result = new BigDecimal(0);
        for(Order order:orderSet) {
            result = result.add(order.getFreeBitcoin());
        }

        return result;
    }

    public Set<Order> filterEffectiveOrder(Set<Order> orderSet) {
        Iterator<Order> orderIterator = orderSet.iterator();
//        Calendar cal = Calendar.getInstance();
//        long today = cal.getTime().getTime();
        while(orderIterator.hasNext()) {
            Order order = orderIterator.next();
//            Date startDate = order.getStartDate();
            Date endDate = order.getEndDate();
            if(endDate != null) orderIterator.remove();
//            if(!(today >= startDate.getTime() && today <= endDate.getTime())) {
//                orderIterator.remove();
//            }
        }

        return orderSet;
    }

    public void modifyOrders(Set<Order> orderSet) {
        orderRepository.saveAll(orderSet);
    }
}
