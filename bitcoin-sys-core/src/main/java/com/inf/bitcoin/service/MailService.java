package com.inf.bitcoin.service;

import com.inf.bitcoin.model.Member;
import com.inf.bitcoin.repository.MemberRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.Calendar;
import java.util.Properties;

/**
 * Created by max on 2018/10/16.
 */
@Service
public class MailService {

    @Value("${spring.mail.host}")
    private String mailHost;

    @Value("${spring.mail.port}")
    private Integer mailPort;

    @Value("${spring.mail.username}")
    private String mailUsername;

    @Value("${spring.mail.password}")
    private String mailPassword;

//    spring.mail.properties.mail.smtp.auth=true
//    spring.mail.properties.mail.smtp.starttls.enable=true
//    spring.mail.properties.mail.smtp.starttls.required=true

//    @Autowired
//    private JavaMailSender mailSender;

//    @Autowired
//    private TemplateEngine templateEngine;

    @Autowired
    private MemberRepository memberRepository;

    public boolean sendForgetPwdMail(Member member, String template, TemplateEngine templateEngine) {
        boolean isSuccess = true;
        String newPwd = RandomStringUtils.randomAlphanumeric(6);
        String encodeNewPwd = new BCryptPasswordEncoder().encode(newPwd);
        member.setPassword(encodeNewPwd);
        member.setHasChangePwd(false);
        try {
            MimeMessagePreparator messagePreparator = mimeMessage -> {
                MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
//            messageHelper.setFrom("sample@dolszewski.com");
                messageHelper.setTo(member.getEmail());
                messageHelper.setSubject("initial-fintech系統密碼通知");

                Context context = new Context();
                context.setVariable("newPwd", newPwd);
                String content = templateEngine.process(template, context);;
                messageHelper.setText(content, true);
                messageHelper.setFrom("service@initial-fintech.com");
                messageHelper.setReplyTo("service@initial-fintech.com");
            };
            getJavaMailSender().send(messagePreparator);

            memberRepository.save(member);
        }catch (Exception e) {
            e.printStackTrace();
            isSuccess = false;
        }

        return isSuccess;
    }

    @Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(mailHost);
        mailSender.setPort(mailPort);

        mailSender.setUsername(mailUsername);
        mailSender.setPassword(mailPassword);

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");

        return mailSender;
    }

    public static void main(String[] args) {
        Calendar cal = Calendar.getInstance();
        cal.roll(Calendar.DAY_OF_MONTH, -1);
        System.out.println(cal.getTime());
        System.out.println(new BCryptPasswordEncoder().encode("123456"));
        System.out.println(RandomStringUtils.randomAlphanumeric(6));
    }

}
