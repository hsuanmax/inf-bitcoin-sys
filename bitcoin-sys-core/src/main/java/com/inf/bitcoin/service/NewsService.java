package com.inf.bitcoin.service;

import com.inf.bitcoin.model.Category;
import com.inf.bitcoin.model.News;
import com.inf.bitcoin.repository.NewsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Files;
import java.util.*;

/**
 * Created by max on 2018/9/28.
 */
@Service
public class NewsService {

    @Autowired
    private NewsRepository newsRepository;

    @Value("${upload.image.path}")
    private String uploadImagePath;

    public void saveNews(News news) {
        newsRepository.save(news);
    }

    public News getNewsForEditoOrShow(Long id) {
        News news = this.getNews(id);
        news.setImageName("image/"+news.getImageName());

        return news;
    }

    public List<News> listNews() {
        List<News> resultList = new ArrayList<News>();
        for(News news:newsRepository.findAll()) {
            news.setImageName("image/"+news.getImageName());
            resultList.add(news);
        }

        return resultList;
    }

    public List<News> listNewsByCategory(Long cid) {
        List<News> resultList = new ArrayList<News>();
        Calendar cal = Calendar.getInstance();
        Date today = cal.getTime();
        Category category = new Category();
        category.setId(cid);
        for(News news:newsRepository.findNewsByCategoryBetweenStartDateAndEndDate(category, today)) {
            news.setImageName("image/"+news.getImageName());
            resultList.add(news);
        }

        return resultList;
    }

    public List<News> listNewsByIndexPage(int count) {
        List<News> resultList = new ArrayList<News>();
        Calendar cal = Calendar.getInstance();
        Date today = cal.getTime();
        int result = 0;
        for(News news:newsRepository.findNewsBetweenStartDateAndEndDate(today)) {
            news.setImageName("image/"+news.getImageName());
            resultList.add(news);
            result++;
            if(result==count)
                break;
        }

        return resultList;
    }

    public News getNews(Long id) {
        return newsRepository.findById(id).get();
    }

    public void delNews(Long id) {
        News news = this.getNews(id);
        newsRepository.delete(news);
    }

    public void uploadImgFile(News news) {
        MultipartFile file = news.getFile();
        File dir = new File(uploadImagePath);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        String contentType = file.getContentType();
        String suffix = "." + contentType.split("/")[1];
        String fileName = UUID.randomUUID().toString()+suffix;
        File serverFile = new File(dir.getAbsolutePath() + File.separator + fileName);

        try {
            try (InputStream is = file.getInputStream();
                 BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile))) {
                int i;
                while ((i = is.read()) != -1) {
                    stream.write(i);
                }
                stream.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        news.setImageName(serverFile.getName());
    }

    public byte[] getImage(String imageName) throws IOException{
        File serverFile = new File(uploadImagePath +File.separator+imageName);

        return Files.readAllBytes(serverFile.toPath());
    }
}
