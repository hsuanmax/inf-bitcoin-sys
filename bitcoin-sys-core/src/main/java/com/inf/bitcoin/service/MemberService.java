package com.inf.bitcoin.service;

import com.inf.bitcoin.bean.form.AdminMemberBean;
import com.inf.bitcoin.model.Hierarchy;
import com.inf.bitcoin.model.Member;
import com.inf.bitcoin.model.Order;
import com.inf.bitcoin.model.Role;
import com.inf.bitcoin.repository.HierarchyRepository;
import com.inf.bitcoin.repository.MemberRepository;
import com.inf.bitcoin.repository.RoleRepository;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by max on 2018/9/8.
 */
@Service
public class MemberService {

    @Autowired
    private MemberRepository memberRepository;
    @Autowired
    private DashboardService dashboardService;
    @Autowired
    private HierarchyRepository hierarchyRepository;
    @Autowired
    private RoleRepository roleRepository;

    public void resetPwdByFirstLogin(Member member) {
        Member memberDb = memberRepository.findById(member.getMid()).get();
        String encodePwd = new BCryptPasswordEncoder().encode(member.getPassword());
        memberDb.setPassword(encodePwd);
        memberDb.setHasChangePwd(true);

        memberRepository.save(memberDb);

    }

    public Member getMemberById(String memberId){
        return memberRepository.findById(memberId).orElse(null);
    }


    public List<Member> list() {
        List<Member> resultList = new ArrayList<Member>();
        memberRepository.findAll().forEach(resultList::add);

        return resultList;
    }

    public List<Member> list(String[] memberIds) {
        List<Member> resultList = new ArrayList<Member>();
        for(String mid:memberIds) {
            resultList.add(this.getMemberById(mid));
        }

        return resultList;
    }

    public void modifyUserInfo(String memberId,String name,String englishName,String email,String editorId){
        Member member = memberRepository.findById(memberId).get();
        Member editor = memberRepository.findById(editorId).get();
        member.setName(name);
        member.setEnglishName(englishName);
        member.setEmail(email);
        member.setEditor(editor);
        member.setUpdateDate(new Date());
        memberRepository.save(member);
    }

    public void modifyWallet(String memberId,String walletCode,String minimumPayout){
        Member member = memberRepository.findById(memberId).get();
        member.setWalletCode(walletCode.trim());
        member.setMinimumPayout(Float.parseFloat(minimumPayout));
        memberRepository.save(member);
    }

    public Map<String,String> modifyPw(String memberId, String oldPw, String newPw){
        Map<String,String> result = new HashMap<>();
        result.put("errorMsg","");

        BCryptPasswordEncoder bcp = new BCryptPasswordEncoder();
        Member member = memberRepository.findById(memberId).get();

        if( !bcp.matches(oldPw, member.getPassword())){
            result.put("errorMsg","舊密碼錯誤。");
            return result;
        }

        member.setPassword(bcp.encode(newPw));
        memberRepository.save(member);
        return result;
    }

    public List<Member> getMemberListByHierarchy(String memberId) {
        Member leader = this.getMemberById(memberId);

        Set<Hierarchy> hierarchySet = leader.getHierarchies();
        Map<String,Member> memberMap = new HashMap<>();
        for(Hierarchy hierarchy:hierarchySet) {
            for(Member member:hierarchy.getMembers()) {
                boolean isLeader = false;
                for(Role role:member.getRoles()){
                    if( "LEADER".equals(role.getRid()) ) isLeader = true;
                }
                if( isLeader && !memberId.equals(member.getMid()) && member.getHierarchies().size()>1)continue;
                if( memberMap.containsKey(member.getMid())) continue;
                BigDecimal totalBitcoin = dashboardService.calculateBitcoinByOwner(member);
                member.setTotalBitcoin(totalBitcoin);
                memberMap.put(member.getMid(),member);
            }
        }

        return new ArrayList(memberMap.values());
    }

    public List<Member> getAdminMemberList(){
        List<Member> result = list();
        for(Member member:result){
            List<Order> orderList = new ArrayList(member.getOrdersForOwner());
            Double sumBoughtBitcoin = orderList.stream().mapToDouble((order)->order.getBoughtBitcoin().doubleValue()).sum();
            Double sumFreeBitcoin = orderList.stream().mapToDouble((order)->order.getFreeBitcoin().doubleValue()).sum();
            member.setTotalBitcoin(new BigDecimal(sumBoughtBitcoin).add(new BigDecimal(sumFreeBitcoin)).setScale(2,BigDecimal.ROUND_DOWN));
        }
        return result;
    }

    public List<Hierarchy> getMemeberHierarchy(String memberId) {
        Member member = this.getMemberById(memberId);
        List<Hierarchy> hierarchyList = new ArrayList<Hierarchy>(member.getHierarchies());

        return hierarchyList;
    }

    public List<Long> getMemberHierarchyIdList(Member member){
        return new ArrayList<>(member.getHierarchies()).stream().map(Hierarchy::getHid).collect(Collectors.toList());
    }

    public List<String> getMemberRoleIdList(Member member){
        return new ArrayList<>(member.getRoles()).stream().map(Role::getRid).collect(Collectors.toList());

    }

    public void createMemberInfo(AdminMemberBean bean,String creatorId){
        Member creator = memberRepository.findById(creatorId).get();
        Member member = new Member();
        member.setMid(bean.getMemberId());
        List<Long> hierarchyList = bean.getHierarchyList();

        if(CollectionUtils.isNotEmpty(hierarchyList)){
            Set<Hierarchy> hierarchies = hierarchyList.stream()
                    .map(id-> hierarchyRepository.findById(id).get())
                    .collect(Collectors.toSet());
            member.setHierarchies(hierarchies);
        }
        if(CollectionUtils.isNotEmpty(hierarchyList) && hierarchyList.size()>2){
            member.setReferrerId("");
        }else{
            member.setReferrerId(bean.getReferrerId());
        }

        member.setName(bean.getName());
        member.setEnglishName(bean.getEnglishName());
        member.setEmail(bean.getEmail());
        member.setCreator(creator);
        member.setCreatedDate(new Date());
        member.setPassword(new BCryptPasswordEncoder().encode("123456"));

        if(CollectionUtils.isNotEmpty(bean.getRoleList())){
            Set<Role> roles = bean.getRoleList().stream()
                    .map(id-> roleRepository.findById(id).get())
                    .collect(Collectors.toSet());
            member.setRoles(roles);
        }else{
            member.setRoles(null);
        }

        memberRepository.save(member);
    }

    public void modifyMemberInfo(AdminMemberBean bean,String editorId){
        Member editor = getMemberById(editorId);
        Member member = getMemberById(bean.getMemberId());

        List<Long> hierarchyList = bean.getHierarchyList();

        Set<Hierarchy> hierarchies = hierarchyList.stream()
                                        .map(id-> hierarchyRepository.findById(id).get())
                                        .collect(Collectors.toSet());
        member.setHierarchies(hierarchies);

        if(CollectionUtils.isNotEmpty(hierarchyList) && hierarchyList.size()>2){
            member.setReferrerId("");
        }else{
            member.setReferrerId(bean.getReferrerId());
        }

        member.setName(bean.getName());
        member.setEnglishName(bean.getEnglishName());
        member.setEmail(bean.getEmail());
        member.setEditor(editor);
        member.setUpdateDate(new Date());

        if(CollectionUtils.isNotEmpty(bean.getRoleList())){
            Set<Role> roles = bean.getRoleList().stream()
                    .map(id-> roleRepository.findById(id).get())
                    .collect(Collectors.toSet());
            member.setRoles(roles);
        }else{
            member.setRoles(null);
        }

        memberRepository.save(member);
    }

    public List<Hierarchy> getHierarchyList() {
        List<Hierarchy> hierarchyList = new ArrayList<Hierarchy>();
        hierarchyRepository.findAll().forEach(hierarchyList::add);
        return hierarchyList;
    }

    public void saveNewGeneralMember(Member member,String creatorId) {
        Member creator = getMemberById(creatorId);
        Set<Role> roles = new HashSet<Role>();
        Role role = new Role();
        role.setRid("GENERAL");
        roles.add(role);
        member.setRoles(roles);
        member.setCreatedDate(new Date());
        member.setPassword(new BCryptPasswordEncoder().encode("123456"));
        member.setCreator(creator);
        memberRepository.save(member);

    }

    public void delMember(String memberId) {
        memberRepository.delete(this.getMemberById(memberId));
    }

    public static void main(String[] args) {
        BCryptPasswordEncoder bcp = new BCryptPasswordEncoder();
        System.out.println(bcp.encode("123456"));
    }

}
