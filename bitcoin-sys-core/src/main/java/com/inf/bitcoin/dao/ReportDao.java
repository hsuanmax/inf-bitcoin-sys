package com.inf.bitcoin.dao;


import com.inf.bitcoin.bean.form.ReportIncomeForm;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class ReportDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Map<String,Object>> getReport(ReportIncomeForm formBean){
        List<String> list = new ArrayList<>();
        StringBuffer sb = new StringBuffer();
        sb.append("SELECT m.mid as memberId,m.name as memberChName,m.englishName as memberEnName,IFNULL(dup.profit,0) as profit,IFNULL(pi.paidCoin,0) as paidCoin,(IFNULL(dup.profit,0)- IFNULL(pi.paidCoin,0)) AS unpay,m.minimumPayout,m.walletCode FROM Member AS m" +
                "                LEFT JOIN MemberHierarchy AS mh ON m.mid = mh.member_id");
        sb.append(" INNER JOIN (SELECT memberId,sum(profit) as profit FROM DailyUnpaidProfit where 1=1 ");
        if(StringUtils.isNotBlank(formBean.getStartDate())){
            sb.append(" and createDate >= STR_TO_DATE(?,'%Y/%m/%d') ");
            list.add(formBean.getStartDate());
        }
        if(StringUtils.isNotBlank(formBean.getEndDate())){
            sb.append(" and createDate < STR_TO_DATE(?,'%Y/%m/%d') ");
            list.add(formBean.getEndDate());
        }
        sb.append(" group by DailyUnpaidProfit.memberId) AS dup ON m.mid=dup.memberId ");

        sb.append(" LEFT JOIN (SELECT memberId,sum(paidCoin) as paidCoin FROM PaidIncome where 1=1 ");
        if(StringUtils.isNotBlank(formBean.getStartDate())){
            sb.append(" and createDate >= STR_TO_DATE(?,'%Y/%m/%d') ");
            list.add(formBean.getStartDate());
        }
        if(StringUtils.isNotBlank(formBean.getEndDate())){
            sb.append(" and createDate < STR_TO_DATE(?,'%Y/%m/%d') ");
            list.add(formBean.getEndDate());
        }
        sb.append(" group by PaidIncome.memberId) AS pi on m.mid = pi.memberId ");
        sb.append(" where 1=1 ");

        if(StringUtils.isNotBlank(formBean.getMemberId())){
            sb.append(" and m.mid like ? ");
            list.add("%"+formBean.getMemberId()+"%");
        }

        if(StringUtils.isNotBlank(formBean.getLeaderId())){
            sb.append(" and m.mid = ? ");
            list.add(formBean.getLeaderId());
        }

        if(StringUtils.isNotBlank(formBean.getHierarchyId())){
            sb.append(" and mh.hierarchy_id= ? ");
            list.add(formBean.getHierarchyId());
        }

        if(formBean.getIsPayout()){
            sb.append(" and (IFNULL(dup.profit,0)- IFNULL(pi.paidCoin,0)) > m.minimumPayout ");
        }
        sb.append(" and m.mid not in ( SELECT member_id FROM MemberHierarchy group by member_id having count(member_id) > 1 ) ");

        return jdbcTemplate.queryForList(sb.toString(),list.stream().toArray());

    }

    
}
