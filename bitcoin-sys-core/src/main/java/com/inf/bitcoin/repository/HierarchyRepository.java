package com.inf.bitcoin.repository;

import com.inf.bitcoin.model.Hierarchy;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by max on 2018/9/21.
 */
@Repository
public interface HierarchyRepository extends CrudRepository<Hierarchy, Long> {
    List<Hierarchy> findByName(String name);
}
