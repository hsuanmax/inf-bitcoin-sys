package com.inf.bitcoin.repository;

import com.inf.bitcoin.model.Member;
import com.inf.bitcoin.model.Order;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Created by max on 2018/9/17.
 */
@Repository
public interface OrderRepository extends CrudRepository<Order, Long> {

    List<Order> findByCreator(Member creator);
    List<Order> findByOwner(Member owner);
}
