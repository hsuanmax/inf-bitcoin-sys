package com.inf.bitcoin.repository;

import com.inf.bitcoin.model.Category;
import com.inf.bitcoin.model.News;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by max on 2018/9/28.
 */
@Repository
public interface NewsRepository extends CrudRepository<News, Long> {

    @Query("SELECT n FROM News n WHERE :today BETWEEN n.startDate AND n.endDate ORDER BY n.createDate DESC ")
    public List<News> findNewsBetweenStartDateAndEndDate(@Param("today") Date today);

    @Query("SELECT n FROM News n WHERE n.category = :category and :today BETWEEN n.startDate AND n.endDate ORDER BY n.createDate DESC ")
    public List<News> findNewsByCategoryBetweenStartDateAndEndDate(@Param("category") Category category, @Param("today") Date today);

}
