package com.inf.bitcoin.repository;

import com.inf.bitcoin.model.SystemConfig;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by max on 2018/10/1.
 */
public interface SystemConfigRepository extends CrudRepository<SystemConfig, String> {
}
