package com.inf.bitcoin.repository;

import com.inf.bitcoin.model.LoginErrorRecord;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by max on 2018/10/10.
 */
@Repository
public interface LoginErrorRecordRepository extends CrudRepository<LoginErrorRecord, Long> {

    List<LoginErrorRecord> findByIpAddress(String ipAddress);
    int countByIpAddressAndErrorDateBetween(String ipAddress, Date startDate, Date endDate);
    List<LoginErrorRecord> findByErrorDateBetween(Date startDate, Date endDate);
}
