package com.inf.bitcoin.repository;

import com.inf.bitcoin.model.IncomeTemp;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IncomeTempRepository extends CrudRepository<IncomeTemp, String> {

    List<IncomeTemp> findByMid(String mid);
}
