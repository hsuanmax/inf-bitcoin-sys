package com.inf.bitcoin.repository;

import com.inf.bitcoin.model.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by max on 2018/9/13.
 */
@Repository
public interface RoleRepository extends CrudRepository<Role, String> {
}
