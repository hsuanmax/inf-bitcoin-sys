package com.inf.bitcoin.repository;

import com.inf.bitcoin.model.Member;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by max on 2018/9/13.
 */
@Repository
public interface MemberRepository extends CrudRepository<Member, String>{
}
