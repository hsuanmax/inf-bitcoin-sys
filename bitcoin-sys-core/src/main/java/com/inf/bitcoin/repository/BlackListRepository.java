package com.inf.bitcoin.repository;

import com.inf.bitcoin.model.BlackList;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by max on 2018/10/10.
 */
@Repository
public interface BlackListRepository extends CrudRepository<BlackList, Long> {

    List<BlackList> findByIpAddress(String ipAddress);
}
