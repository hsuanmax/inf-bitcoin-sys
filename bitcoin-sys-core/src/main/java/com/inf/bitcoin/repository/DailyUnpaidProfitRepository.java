package com.inf.bitcoin.repository;

import com.inf.bitcoin.model.DailyUnpaidProfit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by max on 2018/10/1.
 */
@Repository
public interface DailyUnpaidProfitRepository extends CrudRepository<DailyUnpaidProfit, Long> {
    List<DailyUnpaidProfit> findDailyUnpaidProfitsByMemberIdOrderByCreateDateDesc(String memberId);
}
