package com.inf.bitcoin.repository;

import com.inf.bitcoin.model.PaidIncome;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaidIncomeRepository  extends CrudRepository<PaidIncome, Long> {
    List<PaidIncome> findPaidIncomesByMemberIdOrderByPayDateDesc(String memberId);
}
