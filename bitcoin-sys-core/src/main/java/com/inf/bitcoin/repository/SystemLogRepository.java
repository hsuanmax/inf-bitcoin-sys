package com.inf.bitcoin.repository;

import com.inf.bitcoin.model.SystemLog;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by max on 2018/11/13.
 */
@Repository
public interface SystemLogRepository extends CrudRepository<SystemLog, String> {
}
