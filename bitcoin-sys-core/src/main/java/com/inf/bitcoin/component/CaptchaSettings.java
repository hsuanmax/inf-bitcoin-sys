package com.inf.bitcoin.component;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
//@ConfigurationProperties(prefix = "google.recaptcha.key")
//@ConfigurationProperties(prefix = "luosimao.recaptcha.key")
public class CaptchaSettings {

    @Value("${luosimao.recaptcha.key.site}")
    private String site;

    @Value("${luosimao.recaptcha.key.api}")
    private String api;

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }
}
