package com.inf.bitcoin.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by max on 2018/10/1.
 */
@Entity
public class DailyUnpaidProfit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 15)
    private String memberId;

    private Date createDate;

    @Column(precision = 16, scale = 8)
    private BigDecimal profit;

    @Column(precision = 16, scale = 8)
    private BigDecimal currentCoinsPerHour;

    @Column(precision = 16, scale = 8)
    private BigDecimal currentExchangeRate;

    @Column(precision = 16, scale = 8)
    private BigDecimal freeTotalT;

    @Column(precision = 16, scale = 8)
    private BigDecimal boughtTotalT;

    public Long getId() {
        return id;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public BigDecimal getProfit() {
        return profit;
    }

    public void setProfit(BigDecimal profit) {
        this.profit = profit;
    }

    public BigDecimal getCurrentCoinsPerHour() {
        return currentCoinsPerHour;
    }

    public void setCurrentCoinsPerHour(BigDecimal currentCoinsPerHour) {
        this.currentCoinsPerHour = currentCoinsPerHour;
    }

    public BigDecimal getCurrentExchangeRate() {
        return currentExchangeRate;
    }

    public void setCurrentExchangeRate(BigDecimal currentExchangeRate) {
        this.currentExchangeRate = currentExchangeRate;
    }

    public BigDecimal getFreeTotalT() {
        return freeTotalT;
    }

    public void setFreeTotalT(BigDecimal freeTotalT) {
        this.freeTotalT = freeTotalT;
    }

    public BigDecimal getBoughtTotalT() {
        return boughtTotalT;
    }

    public void setBoughtTotalT(BigDecimal boughtTotalT) {
        this.boughtTotalT = boughtTotalT;
    }
}
