package com.inf.bitcoin.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by max on 2018/10/10.
 */
@Entity
public class LoginErrorRecord {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Date errorDate;

    @Column(length = 30)
    private String ipAddress;

    public Date getErrorDate() {
        return errorDate;
    }

    public void setErrorDate(Date errorDate) {
        this.errorDate = errorDate;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }
}
