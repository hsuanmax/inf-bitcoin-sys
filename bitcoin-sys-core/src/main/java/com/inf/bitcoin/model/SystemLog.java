package com.inf.bitcoin.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by max on 2018/11/13.
 */
@Entity
public class SystemLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 15)
    private String memberId;

    @Enumerated(EnumType.STRING)
    @Column(length = 50)
    private LogType logDesc;

    @Column(length = 50)
    private String oldValue;

    @Column(length = 50)
    private String newValue;

    private Date date;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public LogType getLogDesc() {
        return logDesc;
    }

    public void setLogDesc(LogType logDesc) {
        this.logDesc = logDesc;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
