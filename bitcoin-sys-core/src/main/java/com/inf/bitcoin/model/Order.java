package com.inf.bitcoin.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by max on 2018/9/17.
 */
@Entity
@Table(name="`Order`")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(precision = 13, scale = 2)
    private BigDecimal boughtBitcoin;

    @Column(precision = 13, scale = 2)
    private BigDecimal freeBitcoin;

    @Column(length = 50)
    private String remarks;

    @Column
    private Date createdDate;

    @Column
    private Date updateDate;

    @Column
    private Date startDate;

    @Column
    private Date endDate;

    @ManyToOne
    private Member creator;

    @ManyToOne
    private Member editor;

    @ManyToOne
    private Member owner;

    @Transient
    private BigDecimal totalBitcoin;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getBoughtBitcoin() {
        return boughtBitcoin;
    }

    public void setBoughtBitcoin(BigDecimal boughtBitcoin) {
        this.boughtBitcoin = boughtBitcoin;
    }

    public BigDecimal getFreeBitcoin() {
        return freeBitcoin;
    }

    public void setFreeBitcoin(BigDecimal freeBitcoin) {
        this.freeBitcoin = freeBitcoin;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Member getCreator() {
        return creator;
    }

    public void setCreator(Member creator) {
        this.creator = creator;
    }

    public Member getOwner() {
        return owner;
    }

    public void setOwner(Member owner) {
        this.owner = owner;
    }

    public BigDecimal getTotalBitcoin() {
        return totalBitcoin;
    }

    public void setTotalBitcoin(BigDecimal totalBitcoin) {
        this.totalBitcoin = totalBitcoin;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Member getEditor() {
        return editor;
    }

    public void setEditor(Member editor) {
        this.editor = editor;
    }
}
