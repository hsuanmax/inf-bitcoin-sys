package com.inf.bitcoin.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
public class PaidIncome {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 15)
    private String memberId;

    @Column
    private Date createDate;

    @Column
    private Date payDate;

    @Column(precision = 16, scale = 8)
    private BigDecimal paidCoin;

    @Column(length = 255)
    private String hashValue;

    @Column(length = 255)
    private String payTo;

    public String getPayTo() {
        return payTo;
    }

    public void setPayTo(String payTo) {
        this.payTo = payTo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }

    public BigDecimal getPaidCoin() {
        return paidCoin;
    }

    public void setPaidCoin(BigDecimal paidCoin) {
        this.paidCoin = paidCoin;
    }

    public String getHashValue() {
        return hashValue;
    }

    public void setHashValue(String hashValue) {
        this.hashValue = hashValue;
    }
}
