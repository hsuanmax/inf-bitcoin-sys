package com.inf.bitcoin.model;

import org.hibernate.annotations.Nationalized;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by max on 2018/9/13.
 */
@Entity
public class Role {

    @Id
    @Column(length = 100)
    private String rid;

    @Basic
    @Nationalized
    @Column(length = 50)
    private String name;

    @ManyToMany(mappedBy = "roles")
    private Collection<Member> members;

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<Member> getMembers() {
        return members;
    }

    public void setMembers(Collection<Member> members) {
        this.members = members;
    }
}
