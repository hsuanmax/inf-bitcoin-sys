package com.inf.bitcoin.model;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

/**
 * Created by max on 2018/9/21.
 */
@Entity
public class Hierarchy {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long hid;

    @Column(length = 30)
    private String name;

    @ManyToMany(mappedBy = "hierarchies")
    private Collection<Member> members;

    @Column
    private Date createdDate;

    @Column
    private Date updateDate;

    @ManyToOne
    private Member creator;

    @ManyToOne
    private Member editor;

    public Long getHid() {
        return hid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<Member> getMembers() {
        return members;
    }

    public void setMembers(Collection<Member> members) {
        this.members = members;
    }

    public void setHid(Long hid) {
        this.hid = hid;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Member getCreator() {
        return creator;
    }

    public void setCreator(Member creator) {
        this.creator = creator;
    }

    public Member getEditor() {
        return editor;
    }

    public void setEditor(Member editor) {
        this.editor = editor;
    }
}
