package com.inf.bitcoin.model;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Nationalized;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by max on 2018/9/13.
 */
@Entity
public class Member {

    @Id
    @Column(length = 15)
    private String mid;

    @Column(length = 100)
    private String password;

    @Basic
    @Nationalized
    @Column(length = 50)
    private String name;

    private boolean hasChangePwd;

    @Column(length = 50)
    private String englishName;

    @Column(length = 200)
    
    private String email;

    @Column(length = 15)
    private String referrerId;

    @Column(length = 100)
    private String walletCode;

    @Column(length = 100)
    private Float minimumPayout;

    @OneToMany(mappedBy = "creator")
    private Set<Order> ordersForCreator = new HashSet<Order>();

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "owner")
    private Set<Order> ordersForOwner = new HashSet<Order>();

    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(
            name = "MemberRole",
            joinColumns = @JoinColumn(
                    name = "member_id", referencedColumnName = "mid"),
            inverseJoinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "rid"))
    private Set<Role> roles;

    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(
            name = "MemberHierarchy",
            joinColumns = @JoinColumn(
                    name = "member_id", referencedColumnName = "mid"),
            inverseJoinColumns = @JoinColumn(
                    name = "hierarchy_id", referencedColumnName = "hid"))
    private Set<Hierarchy> hierarchies;

    @ManyToOne
    private Member creator;

    @ManyToOne
    private Member editor;

    @Column
    private Date createdDate;

    @Column
    private Date updateDate;

    @Transient
    private BigDecimal totalBitcoin;
    
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getWalletCode() {
        return walletCode;
    }

    public void setWalletCode(String walletCode) {
        this.walletCode = walletCode;
    }

    public Float getMinimumPayout() {
        return minimumPayout;
    }

    public void setMinimumPayout(Float minimumPayout) {
        this.minimumPayout = minimumPayout;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getReferrerId() {
        return referrerId;
    }

    public void setReferrerId(String referrerId) {
        this.referrerId = referrerId;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public boolean getHasChangePwd() {
        return hasChangePwd;
    }

    public void setHasChangePwd(boolean hasChangePwd) {
        this.hasChangePwd = hasChangePwd;
    }

    public Set<Order> getOrdersForCreator() {
        return ordersForCreator;
    }

    public void setOrdersForCreator(Set<Order> ordersForCreator) {
        this.ordersForCreator = ordersForCreator;
    }

    public Set<Order> getOrdersForOwner() {
        return ordersForOwner;
    }

    public void setOrdersForOwner(Set<Order> ordersForOwner) {
        this.ordersForOwner = ordersForOwner;
    }

    public Set<Hierarchy> getHierarchies() {
        return hierarchies;
    }

    public void setHierarchies(Set<Hierarchy> hierarchies) {
        this.hierarchies = hierarchies;
    }

    public BigDecimal getTotalBitcoin() {
        return totalBitcoin;
    }

    public void setTotalBitcoin(BigDecimal totalBitcoin) {
        this.totalBitcoin = totalBitcoin;
    }

    public Member getCreator() {
        return creator;
    }

    public void setCreator(Member creator) {
        this.creator = creator;
    }

    public Member getEditor() {
        return editor;
    }

    public void setEditor(Member editor) {
        this.editor = editor;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

}
