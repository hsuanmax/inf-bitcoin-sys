package com.inf.bitcoin.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by max on 2018/10/1.
 */
@Entity
public class SystemConfig {

    @Id
    @Column(length = 15)
    private String paramId;

    @Column(length = 30)
    private String paramValue;

    @Column(length = 50)
    private String paramName;

    public String getParamId() {
        return paramId;
    }

    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    public void setParamId(String paramId) {
        this.paramId = paramId;
    }

    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }
}
