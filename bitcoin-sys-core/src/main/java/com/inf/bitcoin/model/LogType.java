package com.inf.bitcoin.model;

/**
 * Created by max on 2018/11/13.
 */
public enum LogType {
    CHANGE_EMAIL,CHANGE_MIN_PAYOUT,CHANGE_WALLET
}
