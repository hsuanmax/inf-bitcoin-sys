package com.inf.bitcoin.config;

import org.springframework.context.annotation.Configuration;

import com.inf.bitcoin.config.JpaConfig;
import org.springframework.context.annotation.*;
import org.springframework.stereotype.Service;

@Configuration
@Import({JpaConfig.class})
@ComponentScan(basePackages = {"com.inf.bitcoin.service","com.inf.bitcoin.component",
        "com.inf.bitcoin.dao", "com.inf.bitcoin.repository"}
        , includeFilters = @ComponentScan.Filter(type = FilterType.ANNOTATION, value = Service.class))
@PropertySource("classpath:application-bitcoin-sys.properties" )
public class CoreConfig {


}