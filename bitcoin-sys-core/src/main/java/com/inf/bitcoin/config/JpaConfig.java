package com.inf.bitcoin.config;

import com.mysql.cj.jdbc.MysqlDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Properties;

@Configuration
@PropertySource("classpath:application-bitcoin-sys.properties")
@EnableJpaRepositories("com.inf.bitcoin.repository")
public class JpaConfig {

	@Autowired
    Environment env;

	@Bean(name = "infDataSource")
	@Primary
	@ConfigurationProperties(prefix = "inf.datasource")
	public DataSource infDataSource() throws SQLException {
		MysqlDataSource ds = new MysqlDataSource();
		ds.setUser(env.getProperty("inf.jdbc.user"));
		ds.setPassword(env.getProperty("inf.jdbc.password"));
		ds.setServerName(env.getProperty("inf.jdbc.server.name"));
		ds.setPortNumber(Integer.valueOf(env.getProperty("inf.jdbc.port.number")));
		ds.setDatabaseName(env.getProperty("inf.jdbc.database.name"));
		ds.setServerTimezone(env.getProperty("inf.jdbc.server.time.zone"));
		ds.setUseSSL(false);
		ds.setCharacterEncoding("UTF-8");
		return ds;
	}

	@Bean(name = "entityManagerFactory")
	@Primary
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(@Qualifier("infDataSource")DataSource infDataSource) {
		LocalContainerEntityManagerFactoryBean entityManagerFactory =
				new LocalContainerEntityManagerFactoryBean();
		entityManagerFactory.setDataSource(infDataSource);
		entityManagerFactory.setPackagesToScan("com.inf.bitcoin.model");
		entityManagerFactory.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
		entityManagerFactory.setJpaProperties(infHibernateProperties());

		return entityManagerFactory;
	}

	@Bean
	public JpaTransactionManager transactionManager(@Qualifier("entityManagerFactory") EntityManagerFactory entityManagerFactory) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory);
		return transactionManager;
	}

	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

	@Bean
	public JdbcTemplate jdbcTemplate(@Qualifier("infDataSource") DataSource dataSource) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.setResultsMapCaseInsensitive(true);

		return jdbcTemplate;
	}



	private Properties infHibernateProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.dialect", getRequiredProperty("inf.hibernate.dialect"));
		properties.put("hibernate.show_sql", getRequiredProperty("inf.hibernate.show_sql"));
		properties.put("hibernate.format_sql", getRequiredProperty("inf.hibernate.format_sql"));
		properties.put("hibernate.hbm2ddl.auto", getRequiredProperty("inf.hibernate.hbm2ddl.auto"));
//		properties.put("hibernate.hbm2ddl.import_files", getRequiredProperty("inf.hibernate.hbm2ddl.import_files"));

		return properties;
	}

	private String getRequiredProperty(String key) {
		return env.getRequiredProperty(key);
	}
}


