package com.inf.bitcoin.bean.json;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CpListingsLatest {

    @JsonProperty("status")
    private CpStatus status;

    @JsonProperty("data")
    private List<CryptocurrencyPrices> data;

    public CpStatus getStatus() {
        return status;
    }

    public void setStatus(CpStatus status) {
        this.status = status;
    }

    public List<CryptocurrencyPrices> getData() {
        return data;
    }

    public void setData(List<CryptocurrencyPrices> data) {
        this.data = data;
    }
}
