package com.inf.bitcoin.bean.response;

public class MemberIncomInfo {
    private String paidBtc;
    private String unpaidBtc;
    private String historyBtc;

    public String getPaidBtc() {
        return paidBtc;
    }

    public void setPaidBtc(String paidBtc) {
        this.paidBtc = paidBtc;
    }

    public String getUnpaidBtc() {
        return unpaidBtc;
    }

    public void setUnpaidBtc(String unpaidBtc) {
        this.unpaidBtc = unpaidBtc;
    }

    public String getHistoryBtc() {
        return historyBtc;
    }

    public void setHistoryBtc(String historyBtc) {
        this.historyBtc = historyBtc;
    }
}
