package com.inf.bitcoin.bean.json;

/**
 * Created by max on 2018/10/27.
 */
public class LuosimaoResponse {

    private String error;
    private String res;
    private String msg;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getRes() {
        return res;
    }

    public void setRes(String res) {
        this.res = res;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
