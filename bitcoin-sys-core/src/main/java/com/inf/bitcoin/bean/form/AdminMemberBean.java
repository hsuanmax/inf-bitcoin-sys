package com.inf.bitcoin.bean.form;

import java.util.List;

public class AdminMemberBean {
    private String memberId;
    private List<Long> hierarchyList;
    private String referrerId;
    private String name;
    private String englishName;
    private String email;
    private List<String> roleList;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public List<Long> getHierarchyList() {
        return hierarchyList;
    }

    public void setHierarchyList(List<Long> hierarchyList) {
        this.hierarchyList = hierarchyList;
    }

    public String getReferrerId() {
        return referrerId;
    }

    public void setReferrerId(String referrerId) {
        this.referrerId = referrerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<String> roleList) {
        this.roleList = roleList;
    }

    @Override
    public String toString() {
        return "AdminMemberBean{" +
                "memberId='" + memberId + '\'' +
                ", hierarchyList=" + hierarchyList +
                ", referrerId='" + referrerId + '\'' +
                ", name='" + name + '\'' +
                ", englishName='" + englishName + '\'' +
                ", email='" + email + '\'' +
                ", roleList=" + roleList +
                '}';
    }
}
