package com.inf.bitcoin.bean.form;

public class ReportIncomeForm {
    private String leaderId;
    private String hierarchyId;
    private String memberId;
    private String startDate;
    private String endDate;
    private boolean isPayout;

    public boolean getIsPayout() {
        return isPayout;
    }

    public void setIsPayout(Boolean payout) {
        isPayout = payout;
    }

    public String getLeaderId() {
        return leaderId;
    }

    public void setLeaderId(String leaderId) {
        this.leaderId = leaderId;
    }

    public String getHierarchyId() {
        return hierarchyId;
    }

    public void setHierarchyId(String hierarchyId) {
        this.hierarchyId = hierarchyId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
