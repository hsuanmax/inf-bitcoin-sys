package com.inf.bitcoin.bean.json;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.Map;

public class CryptocurrencyPrices {
    @JsonProperty("id")
    private Long id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("symbol")
    private String symbol;

    @JsonProperty("slug")
    private String slug;

    @JsonProperty("circulating_supply")
    private BigDecimal circulatingSupply;

    @JsonProperty("total_supply")
    private BigDecimal totalSupply;
    @JsonProperty("max_supply")
    private BigDecimal maxSupply;
    @JsonProperty("date_added")
    private String dateAdded;
    @JsonProperty("num_market_pairs")
    private BigDecimal numMarketPairs;
    @JsonProperty("cmc_rank")
    private BigDecimal cmcRank;
    @JsonProperty("last_updated")
    private String lastUpdated;
    @JsonProperty("quote")
    private Map<String,Quote> quote;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public BigDecimal getCirculatingSupply() {
        return circulatingSupply;
    }

    public void setCirculatingSupply(BigDecimal circulatingSupply) {
        this.circulatingSupply = circulatingSupply;
    }

    public BigDecimal getTotalSupply() {
        return totalSupply;
    }

    public void setTotalSupply(BigDecimal totalSupply) {
        this.totalSupply = totalSupply;
    }

    public BigDecimal getMaxSupply() {
        return maxSupply;
    }

    public void setMaxSupply(BigDecimal maxSupply) {
        this.maxSupply = maxSupply;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public BigDecimal getNumMarketPairs() {
        return numMarketPairs;
    }

    public void setNumMarketPairs(BigDecimal numMarketPairs) {
        this.numMarketPairs = numMarketPairs;
    }

    public BigDecimal getCmcRank() {
        return cmcRank;
    }

    public void setCmcRank(BigDecimal cmcRank) {
        this.cmcRank = cmcRank;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Map<String, Quote> getQuote() {
        return quote;
    }

    public void setQuote(Map<String, Quote> quote) {
        this.quote = quote;
    }
}
