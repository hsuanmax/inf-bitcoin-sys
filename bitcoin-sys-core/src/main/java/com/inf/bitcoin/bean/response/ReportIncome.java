package com.inf.bitcoin.bean.response;

import java.math.BigDecimal;

public class ReportIncome {
    private String memberChName;
    private String memberEnName;
    private String memberId;
    private BigDecimal profit;
    private BigDecimal paidCoin;
    private BigDecimal unpay;
    private Float minimumPayout;
    private String walletCode;

    public String getMemberChName() {
        return memberChName;
    }

    public void setMemberChName(String memberChName) {
        this.memberChName = memberChName;
    }

    public String getMemberEnName() {
        return memberEnName;
    }

    public void setMemberEnName(String memberEnName) {
        this.memberEnName = memberEnName;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public BigDecimal getProfit() {
        return profit;
    }

    public void setProfit(BigDecimal profit) {
        this.profit = profit;
    }

    public BigDecimal getPaidCoin() {
        return paidCoin;
    }

    public void setPaidCoin(BigDecimal paidCoin) {
        this.paidCoin = paidCoin;
    }

    public BigDecimal getUnpay() {
        return unpay;
    }

    public void setUnpay(BigDecimal unpay) {
        this.unpay = unpay;
    }

    public Float getMinimumPayout() {
        return minimumPayout;
    }

    public void setMinimumPayout(Float minimumPayout) {
        this.minimumPayout = minimumPayout;
    }

    public String getWalletCode() {
        return walletCode;
    }

    public void setWalletCode(String walletCode) {
        this.walletCode = walletCode;
    }
}
