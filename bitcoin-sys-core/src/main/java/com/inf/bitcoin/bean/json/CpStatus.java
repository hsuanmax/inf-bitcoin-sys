package com.inf.bitcoin.bean.json;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CpStatus {
    @JsonProperty("timestamp")
    private String timestamp;
    @JsonProperty("error_code")
    private Long errorCode;
    @JsonProperty("error_message")
    private String errorMessage;
    @JsonProperty("elapsed")
    private Long elapsed;
    @JsonProperty("credit_count")
    private Long creditCount;

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Long getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Long errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Long getElapsed() {
        return elapsed;
    }

    public void setElapsed(Long elapsed) {
        this.elapsed = elapsed;
    }

    public Long getCreditCount() {
        return creditCount;
    }

    public void setCreditCount(Long creditCount) {
        this.creditCount = creditCount;
    }
}
