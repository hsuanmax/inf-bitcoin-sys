package com.inf.bitcoin.jobs;

import com.inf.bitcoin.batch.BitcoinCalculatorAPIReader;
import com.inf.bitcoin.batch.CalculateUnpaidProfitProcessor;
import com.inf.bitcoin.batch.DailyUnpaidProfitWriter;
import com.inf.bitcoin.config.JpaConfig;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Created by max on 2018/10/1.
 */
@Configuration
@Import(JpaConfig.class)
public class DailyUnpaidProfitJob {

    @Autowired
    public JobBuilderFactory jobs;

    @Autowired
    public StepBuilderFactory steps;

    @Bean
    protected Step callBitcoinCalculatorAPI() {
        return steps
                .get("callBitcoinCalculatorAPI")
                .tasklet(bitcoinCalculatorAPIReader())
                .build();
    }

    @Bean
    protected Step calculateUnpaidProfit() {
        return steps
                .get("calculateUnpaidProfit")
                .tasklet(calculateUnpaidProfitProcessor())
                .build();
    }

    @Bean
    protected Step writeDailyUnpaidProfit() {
        return steps
                .get("writeDailyUnpaidProfit")
                .tasklet(dailyUnpaidProfitWriter())
                .build();
    }

    @Bean
    public Job job() {
        return jobs
                .get("dailyUnpaidProfitJob")
                .incrementer(new RunIdIncrementer())
                .flow(callBitcoinCalculatorAPI())
                .next(calculateUnpaidProfit())
                .next(writeDailyUnpaidProfit())
                .end()
                .build();
    }

    @Bean
    public BitcoinCalculatorAPIReader bitcoinCalculatorAPIReader() {
        return new BitcoinCalculatorAPIReader();
    }

    @Bean
    public CalculateUnpaidProfitProcessor calculateUnpaidProfitProcessor() {
        return new CalculateUnpaidProfitProcessor();
    }

    @Bean
    public DailyUnpaidProfitWriter dailyUnpaidProfitWriter() {
        return new DailyUnpaidProfitWriter();
    }
}
