package com.inf.bitcoin;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.MapJobRepositoryFactoryBean;
import org.springframework.batch.support.transaction.ResourcelessTransactionManager;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * Created by max on 2018/1/31.
 */

@EnableBatchProcessing
@SpringBootApplication
public class BitcoinSysBatchApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(BitcoinSysBatchApplication.class, args);
    }

}
