package com.inf.bitcoin.bean;

import java.math.BigDecimal;

/**
 * Created by max on 2018/9/30.
 */
public class CoinsResponse {

    private BigDecimal bc_per_block;
    private BigDecimal coins_before_retarget;
    private BigDecimal coins_per_hour;
    private BigDecimal coins_per_hour_after_retarget;
    private BigDecimal difficulty;
    private BigDecimal dollars_before_retarget;
    private BigDecimal dollars_per_hour;
    private BigDecimal dollars_per_hour_after_retarget;
    private BigDecimal exchange_rate;
    private String exchange_rate_source;
    private BigDecimal hashrate;
    private BigDecimal next_difficulty;
    private BigDecimal time_per_block;

    public BigDecimal getBc_per_block() {
        return bc_per_block;
    }

    public void setBc_per_block(BigDecimal bc_per_block) {
        this.bc_per_block = bc_per_block;
    }

    public BigDecimal getCoins_before_retarget() {
        return coins_before_retarget;
    }

    public void setCoins_before_retarget(BigDecimal coins_before_retarget) {
        this.coins_before_retarget = coins_before_retarget;
    }

    public BigDecimal getCoins_per_hour() {
        return coins_per_hour;
    }

    public void setCoins_per_hour(BigDecimal coins_per_hour) {
        this.coins_per_hour = coins_per_hour;
    }

    public BigDecimal getCoins_per_hour_after_retarget() {
        return coins_per_hour_after_retarget;
    }

    public void setCoins_per_hour_after_retarget(BigDecimal coins_per_hour_after_retarget) {
        this.coins_per_hour_after_retarget = coins_per_hour_after_retarget;
    }

    public BigDecimal getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(BigDecimal difficulty) {
        this.difficulty = difficulty;
    }

    public BigDecimal getDollars_before_retarget() {
        return dollars_before_retarget;
    }

    public void setDollars_before_retarget(BigDecimal dollars_before_retarget) {
        this.dollars_before_retarget = dollars_before_retarget;
    }

    public BigDecimal getDollars_per_hour() {
        return dollars_per_hour;
    }

    public void setDollars_per_hour(BigDecimal dollars_per_hour) {
        this.dollars_per_hour = dollars_per_hour;
    }

    public BigDecimal getDollars_per_hour_after_retarget() {
        return dollars_per_hour_after_retarget;
    }

    public void setDollars_per_hour_after_retarget(BigDecimal dollars_per_hour_after_retarget) {
        this.dollars_per_hour_after_retarget = dollars_per_hour_after_retarget;
    }

    public BigDecimal getExchange_rate() {
        return exchange_rate;
    }

    public void setExchange_rate(BigDecimal exchange_rate) {
        this.exchange_rate = exchange_rate;
    }

    public String getExchange_rate_source() {
        return exchange_rate_source;
    }

    public void setExchange_rate_source(String exchange_rate_source) {
        this.exchange_rate_source = exchange_rate_source;
    }

    public BigDecimal getHashrate() {
        return hashrate;
    }

    public void setHashrate(BigDecimal hashrate) {
        this.hashrate = hashrate;
    }

    public BigDecimal getNext_difficulty() {
        return next_difficulty;
    }

    public void setNext_difficulty(BigDecimal next_difficulty) {
        this.next_difficulty = next_difficulty;
    }

    public BigDecimal getTime_per_block() {
        return time_per_block;
    }

    public void setTime_per_block(BigDecimal time_per_block) {
        this.time_per_block = time_per_block;
    }
}
