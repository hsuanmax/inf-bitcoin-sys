package com.inf.bitcoin.batch;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.inf.bitcoin.bean.CoinsResponse;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

/**
 * Created by max on 2018/10/2.
 */
public class BitcoinCalculatorAPIReader implements Tasklet, StepExecutionListener {

    private String apiUrl = "https://alloscomp.com/bitcoin/calculator/json?hashrate=1000000000000";
    private RestTemplate restTemplate = new RestTemplate();
    private CoinsResponse coinsResponse;

    @Override
    public void beforeStep(StepExecution stepExecution) {
        coinsResponse = new CoinsResponse();
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        stepExecution
                .getJobExecution()
                .getExecutionContext()
                .put("coinsResponse", this.coinsResponse);

        return ExitStatus.COMPLETED;
    }

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        ResponseEntity<String> response = restTemplate.getForEntity(apiUrl, String.class);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            coinsResponse = objectMapper.readValue(response.getBody(), CoinsResponse.class);
        }catch (IOException e) {
            e.printStackTrace();
        }

        return RepeatStatus.FINISHED;
    }
}
