package com.inf.bitcoin.batch;

import com.inf.bitcoin.bean.CoinsResponse;
import com.inf.bitcoin.model.DailyUnpaidProfit;
import com.inf.bitcoin.model.Member;
import com.inf.bitcoin.model.Order;
import com.inf.bitcoin.service.MemberService;
import com.inf.bitcoin.service.OrderService;
import com.inf.bitcoin.service.SystemConfigService;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by max on 2018/10/2.
 */
public class CalculateUnpaidProfitProcessor implements Tasklet, StepExecutionListener {

    private List<DailyUnpaidProfit> dailyUnpaidProfitList;
    private List<Member> memberList;
    private CoinsResponse coinsResponse;
    private BigDecimal x;
    private String[] rerunMembers = {"jpshirakei83", "jpsetuko1628", "jpsmartSS01"};

    private static final String baseCostStr = "baseCost";

    @Autowired
    private MemberService memberService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private SystemConfigService systemConfigService;

    @Override
    public void beforeStep(StepExecution stepExecution) {
        dailyUnpaidProfitList = new ArrayList<>();
        memberList = memberService.list();
        BigDecimal baseCost = new BigDecimal(1).subtract(new BigDecimal(systemConfigService.findByParamId(baseCostStr)));
//        memberList = memberService.list(rerunMembers);
        ExecutionContext executionContext = stepExecution
                .getJobExecution()
                .getExecutionContext();
        this.coinsResponse = (CoinsResponse) executionContext.get("coinsResponse");
//        this.coinsResponse.setCoins_per_hour(new BigDecimal(0.000001458643256798912));
//        this.coinsResponse.setExchange_rate(new BigDecimal(6514.03));
        x = this.coinsResponse.getCoins_per_hour().multiply(new BigDecimal(24)).multiply(baseCost);
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        stepExecution
                .getJobExecution()
                .getExecutionContext()
                .put("dailyUnpaidProfitList", this.dailyUnpaidProfitList);

        return ExitStatus.COMPLETED;
    }

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        Calendar cal = Calendar.getInstance();
        int month = cal.get(Calendar.MONTH);
//        cal.set(Calendar.DAY_OF_MONTH, 25);
        cal.add(Calendar.DAY_OF_MONTH, -1);
        for(Member member:memberList) {
            Set<Order> orderSet = orderService.filterEffectiveOrder(member.getOrdersForOwner());
            if(orderSet.size()==0) continue;
            DailyUnpaidProfit dailyUnpaidProfit = new DailyUnpaidProfit();
            dailyUnpaidProfit.setCreateDate(cal.getTime());
            dailyUnpaidProfit.setCurrentCoinsPerHour(coinsResponse.getCoins_per_hour());
            dailyUnpaidProfit.setCurrentExchangeRate(coinsResponse.getExchange_rate());
            dailyUnpaidProfit.setMemberId(member.getMid());
            dailyUnpaidProfit.setBoughtTotalT(orderService.getBoughtTotalT(orderSet));
            dailyUnpaidProfit.setFreeTotalT(orderService.getFreeTotalT(orderSet));
            switch (month){
                case Calendar.JANUARY:
                    dailyUnpaidProfitList.add(this.calculateByJan(dailyUnpaidProfit));
                    break;
                case Calendar.FEBRUARY:
                    dailyUnpaidProfitList.add(this.calculateByFeb(dailyUnpaidProfit));
                    break;
                case Calendar.MARCH:
                    dailyUnpaidProfitList.add(this.calculateByMar(dailyUnpaidProfit));
                    break;
                case Calendar.APRIL:
                    dailyUnpaidProfitList.add(this.calculateByApr(dailyUnpaidProfit));
                    break;
                case Calendar.MAY:
                case Calendar.JUNE:
                case Calendar.JULY:
                case Calendar.AUGUST:
                    dailyUnpaidProfitList.add(this.calculateByMJJA(dailyUnpaidProfit));
                    break;
                case Calendar.SEPTEMBER:
                case Calendar.OCTOBER:
                case Calendar.NOVEMBER:
                    dailyUnpaidProfitList.add(this.calculateBySON(dailyUnpaidProfit));
                    break;
                case Calendar.DECEMBER:
                    dailyUnpaidProfitList.add(this.calculateByD(dailyUnpaidProfit));
                    break;
            }
        }

        return RepeatStatus.FINISHED;
    }

    private DailyUnpaidProfit calculateByJan(DailyUnpaidProfit dailyUnpaidProfit) {
        BigDecimal commission = new BigDecimal(0.05).divide(coinsResponse.getExchange_rate());
        BigDecimal a = x.subtract(commission);
        int result = a.compareTo(BigDecimal.ZERO);
        if(result == -1) {
            this.makeOrderExpire(dailyUnpaidProfit.getMemberId());
        }

        BigDecimal commissionFree = new BigDecimal(0.22).divide(coinsResponse.getExchange_rate());
        BigDecimal b = x.subtract(commissionFree);

        BigDecimal boughtProfit = dailyUnpaidProfit.getBoughtTotalT().multiply(a).setScale(8, BigDecimal.ROUND_HALF_UP);
        BigDecimal freeProfit = dailyUnpaidProfit.getFreeTotalT().multiply(b).setScale(8, BigDecimal.ROUND_HALF_UP);

        dailyUnpaidProfit.setBoughtTotalT(boughtProfit);
        dailyUnpaidProfit.setFreeTotalT(freeProfit);
        dailyUnpaidProfit.setProfit(boughtProfit.add(freeProfit));

        return dailyUnpaidProfit;

    }

    private DailyUnpaidProfit calculateByFeb(DailyUnpaidProfit dailyUnpaidProfit) {
        BigDecimal commission = new BigDecimal(0.1).divide(coinsResponse.getExchange_rate());
        BigDecimal a = x.subtract(commission);
        int result = a.compareTo(BigDecimal.ZERO);
        if(result == -1) {
            this.makeOrderExpire(dailyUnpaidProfit.getMemberId());
        }

        BigDecimal commissionFree = new BigDecimal(0.22).divide(coinsResponse.getExchange_rate());
        BigDecimal b = x.subtract(commissionFree);

        BigDecimal boughtProfit = dailyUnpaidProfit.getBoughtTotalT().multiply(a).setScale(8, BigDecimal.ROUND_HALF_UP);
        BigDecimal freeProfit = dailyUnpaidProfit.getFreeTotalT().multiply(b).setScale(8, BigDecimal.ROUND_HALF_UP);

        dailyUnpaidProfit.setBoughtTotalT(boughtProfit);
        dailyUnpaidProfit.setFreeTotalT(freeProfit);
        dailyUnpaidProfit.setProfit(boughtProfit.add(freeProfit));

        return dailyUnpaidProfit;

    }

    private DailyUnpaidProfit calculateByMar(DailyUnpaidProfit dailyUnpaidProfit) {
        BigDecimal commission = new BigDecimal(0.15).divide(coinsResponse.getExchange_rate());
        BigDecimal a = x.subtract(commission);
        int result = a.compareTo(BigDecimal.ZERO);
        if(result == -1) {
            this.makeOrderExpire(dailyUnpaidProfit.getMemberId());
        }

        BigDecimal commissionFree = new BigDecimal(0.22).divide(coinsResponse.getExchange_rate());
        BigDecimal b = x.subtract(commissionFree);

        BigDecimal boughtProfit = dailyUnpaidProfit.getBoughtTotalT().multiply(a).setScale(8, BigDecimal.ROUND_HALF_UP);
        BigDecimal freeProfit = dailyUnpaidProfit.getFreeTotalT().multiply(b).setScale(8, BigDecimal.ROUND_HALF_UP);

        dailyUnpaidProfit.setBoughtTotalT(boughtProfit);
        dailyUnpaidProfit.setFreeTotalT(freeProfit);
        dailyUnpaidProfit.setProfit(boughtProfit.add(freeProfit));

        return dailyUnpaidProfit;

    }

    private DailyUnpaidProfit calculateByApr(DailyUnpaidProfit dailyUnpaidProfit) {
        BigDecimal commission = new BigDecimal(0.2).divide(coinsResponse.getExchange_rate());
        BigDecimal a = x.subtract(commission);
        int result = a.compareTo(BigDecimal.ZERO);
        if(result == -1) {
            this.makeOrderExpire(dailyUnpaidProfit.getMemberId());
        }

        BigDecimal commissionFree = new BigDecimal(0.22).divide(coinsResponse.getExchange_rate());
        BigDecimal b = x.subtract(commissionFree);

        BigDecimal boughtProfit = dailyUnpaidProfit.getBoughtTotalT().multiply(a).setScale(8, BigDecimal.ROUND_HALF_UP);
        BigDecimal freeProfit = dailyUnpaidProfit.getFreeTotalT().multiply(b).setScale(8, BigDecimal.ROUND_HALF_UP);

        dailyUnpaidProfit.setBoughtTotalT(boughtProfit);
        dailyUnpaidProfit.setFreeTotalT(freeProfit);
        dailyUnpaidProfit.setProfit(boughtProfit.add(freeProfit));

        return dailyUnpaidProfit;

    }

    private DailyUnpaidProfit calculateByMJJA(DailyUnpaidProfit dailyUnpaidProfit) {
        BigDecimal commission = new BigDecimal(0.22).divide(coinsResponse.getExchange_rate());
        BigDecimal a = x.subtract(commission);
        int result = a.compareTo(BigDecimal.ZERO);
        if(result == -1) {
            this.makeOrderExpire(dailyUnpaidProfit.getMemberId());
        }

        BigDecimal commissionFree = new BigDecimal(0.22).divide(coinsResponse.getExchange_rate());
        BigDecimal b = x.subtract(commissionFree);

        BigDecimal boughtProfit = dailyUnpaidProfit.getBoughtTotalT().multiply(a).setScale(8, BigDecimal.ROUND_HALF_UP);
        BigDecimal freeProfit = dailyUnpaidProfit.getFreeTotalT().multiply(b).setScale(8, BigDecimal.ROUND_HALF_UP);

        dailyUnpaidProfit.setBoughtTotalT(boughtProfit);
        dailyUnpaidProfit.setFreeTotalT(freeProfit);
        dailyUnpaidProfit.setProfit(boughtProfit.add(freeProfit));

        return dailyUnpaidProfit;

    }

    private DailyUnpaidProfit calculateBySON(DailyUnpaidProfit dailyUnpaidProfit) {
        BigDecimal a = x.add(new BigDecimal(0.000006));
        BigDecimal b = x;

        BigDecimal boughtProfit = dailyUnpaidProfit.getBoughtTotalT().multiply(a).setScale(8, BigDecimal.ROUND_HALF_UP);
        BigDecimal freeProfit = dailyUnpaidProfit.getFreeTotalT().multiply(b).setScale(8, BigDecimal.ROUND_HALF_UP);

        dailyUnpaidProfit.setBoughtTotalT(boughtProfit);
        dailyUnpaidProfit.setFreeTotalT(freeProfit);
        dailyUnpaidProfit.setProfit(boughtProfit.add(freeProfit));

        return dailyUnpaidProfit;
    }

    private DailyUnpaidProfit calculateByD(DailyUnpaidProfit dailyUnpaidProfit) {
        BigDecimal a = x;
        BigDecimal b = x;

        BigDecimal boughtProfit = dailyUnpaidProfit.getBoughtTotalT().multiply(a).setScale(8, BigDecimal.ROUND_HALF_UP);
        BigDecimal freeProfit = dailyUnpaidProfit.getFreeTotalT().multiply(b).setScale(8, BigDecimal.ROUND_HALF_UP);

        dailyUnpaidProfit.setBoughtTotalT(boughtProfit);
        dailyUnpaidProfit.setFreeTotalT(freeProfit);
        dailyUnpaidProfit.setProfit(boughtProfit.add(freeProfit));

        return dailyUnpaidProfit;

    }

    private void makeOrderExpire(String memberId) {
        Member member = memberService.getMemberById(memberId);
        Set<Order> orderSet = member.getOrdersForOwner();
        Iterator<Order> orderIterator = orderSet.iterator();
        Calendar cal = Calendar.getInstance();
        Date endDate = cal.getTime();
        while (orderIterator.hasNext()) {
            Order order = orderIterator.next();
            order.setEndDate(endDate);
        }

        orderService.modifyOrders(orderSet);
    }

}
