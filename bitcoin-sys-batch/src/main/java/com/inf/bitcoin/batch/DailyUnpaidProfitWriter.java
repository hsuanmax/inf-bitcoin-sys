package com.inf.bitcoin.batch;

import com.inf.bitcoin.bean.CoinsResponse;
import com.inf.bitcoin.model.DailyUnpaidProfit;
import com.inf.bitcoin.repository.DailyUnpaidProfitRepository;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by max on 2018/10/2.
 */
public class DailyUnpaidProfitWriter implements Tasklet, StepExecutionListener {

    @Autowired
    private DailyUnpaidProfitRepository dailyUnpaidProfitRepository;

    private List<DailyUnpaidProfit> dailyUnpaidProfitList;

    @Override
    public void beforeStep(StepExecution stepExecution) {
        dailyUnpaidProfitList = new ArrayList<>();
        ExecutionContext executionContext = stepExecution
                .getJobExecution()
                .getExecutionContext();
        this.dailyUnpaidProfitList = (List<DailyUnpaidProfit>) executionContext.get("dailyUnpaidProfitList");
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        return ExitStatus.COMPLETED;
    }

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        dailyUnpaidProfitRepository.saveAll(dailyUnpaidProfitList);

        return RepeatStatus.FINISHED;
    }
}
