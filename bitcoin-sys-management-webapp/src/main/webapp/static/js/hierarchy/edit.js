


$(document).on('nifty.ready', function() {
    var faIcon = {
        valid: 'fa fa-check-circle fa-lg text-success',
        invalid: 'fa fa-times-circle fa-lg',
        validating: 'fa fa-refresh'
    }


    $('#editForm').bootstrapValidator({
        message: 'This value is not valid',
        excluded: ':disabled',
        feedbackIcons: faIcon,
        fields: {
            name:{
                validators: {
                    notEmpty: {
                        message: '此欄位不得為空'
                    },
                    remote: {
                        message: '此體系名稱已經存在',
                        method: 'GET',
                        url: 'adminHierarchyCheckName',
                    }
                }
            }
        }
    }).on('status.field.bv', function(e, data) {
        var $form = $(e.target),
            validator = data.bv,
            $collapsePane = data.element.parents('.collapse'),
            colId = $collapsePane.attr('id');

        if (colId) {
            var $anchor = $('a[href="#' + colId + '"][data-toggle="collapse"]');
            var $icon = $anchor.find('i');

            // Add custom class to panel containing the field
            if (data.status == validator.STATUS_INVALID) {
                $anchor.addClass('bv-col-error');
                $icon.removeClass(faIcon.valid).addClass(faIcon.invalid);
            } else if (data.status == validator.STATUS_VALID) {
                var isValidCol = validator.isValidContainer($collapsePane);
                if (isValidCol) {
                    $anchor.removeClass('bv-col-error');
                }else{
                    $anchor.addClass('bv-col-error');
                }
                $icon.removeClass(faIcon.valid + " " + faIcon.invalid).addClass(isValidCol ? faIcon.valid : faIcon.invalid);
            }
        }
    });

});




