
$(document).ready(function() {

    $.fn.DataTable.ext.pager.numbers_length = 5;

    var t = $('#demo-dt-addrow').DataTable({
        "responsive": true,
        "language": {
            "paginate": {
                "previous": '<i class="psi-arrow-left"></i>',
                "next": '<i class="psi-arrow-right"></i>'
            }
        },
        "dom": '<"newtoolbar">frtip',
        "columns": [
            { "width": "5%" },
            null,
            { "width": "12%" }
        ]
    });
    $('#demo-custom-toolbar2').appendTo($("div.newtoolbar"));


});
