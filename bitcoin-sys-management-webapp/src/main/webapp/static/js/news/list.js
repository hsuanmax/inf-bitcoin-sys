$(document).ready(function() {

    $('#news-list-dt').dataTable( {
        "responsive": true,
        "language": {
            "paginate": {
                "previous": '<i class="psi-arrow-left"></i>',
                "next": '<i class="psi-arrow-right"></i>'
            }
        },
        "order": [[ 7, "desc" ]]
    } );

});
