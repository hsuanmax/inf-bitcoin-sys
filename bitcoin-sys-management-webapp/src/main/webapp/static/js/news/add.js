$(document).on('nifty.ready', function() {
    $('#demo-dp-range .input-daterange').datepicker({
        language:  'zh-TW',
        format: "yyyy/mm/dd",
        todayBtn: "linked",
        autoclose: true,
        todayHighlight: true
    });
    $('#adImg').on('change', function(){

        if($("#dynamic"))$("#dynamic").remove();
        var files = document.getElementById('adImg').files;
        if (files.length > 0) {
            var file = files[0];
            getBase64(file).then(
                data => createImg(data)
            );

        }
    });

    function createImg(data){
        var img = $('<img id="dynamic">');
        img.attr('src', data);
        img.attr('style', "width:100%;");
        $('#imgDiv').append(img);
    }
    
    function getBase64(file) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        });
    
    }

    $('#content').summernote({
        height : '230px',
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
        ],
        callbacks: {
            onChange: function(contents, $editable) {
                if (contents != "<p><br></p>"){
                    $("#content").html(contents);
                }
            }
        }
    });




    var faIcon = {
        valid: 'fa fa-check-circle fa-lg text-success',
        invalid: 'fa fa-times-circle fa-lg',
        validating: 'fa fa-refresh'
    };

    $('#add-form').bootstrapValidator({
        excluded: ':disabled',
        feedbackIcons: faIcon,
        fields: {
            subject: {
                validators: {
                    notEmpty: {
                        message: '此欄位不得為空'
                    }
                }
            },
            secSubject: {
                validators: {
                    notEmpty: {
                        message: '此欄位不得為空'
                    }
                }
            },
            startDate: {
                trigger:'change',
                validators: {
                    notEmpty: {
                        message: '此欄位不得為空'
                    }
                }
            },
            endDate: {
                trigger:'change',
                validators: {
                    notEmpty: {
                        message: '此欄位不得為空'
                    }
                }
            },
            category: {
                validators: {
                    callback: {
                        message: '請進行選擇',
                        callback: function(value, validator) {
                            var options = validator.getFieldElements('category').val();
                            return (options != null && options.length != 0);
                        }
                    }
                }
            },
            file:{
                validators: {
                    file: {
                        extension: 'jpg,jpeg,png',
                        type: 'image/jpeg,image/png',
                        maxSize: 2048 * 1024,
                        message: '請選擇2MB的jpg或png檔'
                    }
                }
            },
            tag:{
                validators: {
                    notEmpty: {
                        message: '此欄位不得為空'
                    }
                }
            }
        }
    }).on('status.field.bv', function(e, data) {
        var $form = $(e.target),
            validator = data.bv,
            $collapsePane = data.element.parents('.collapse'),
            colId = $collapsePane.attr('id');

        if (colId) {
            var $anchor = $('a[href="#' + colId + '"][data-toggle="collapse"]');
            var $icon = $anchor.find('i');

            // Add custom class to panel containing the field
            if (data.status == validator.STATUS_INVALID) {
                $anchor.addClass('bv-col-error');
                $icon.removeClass(faIcon.valid).addClass(faIcon.invalid);
            } else if (data.status == validator.STATUS_VALID) {
                var isValidCol = validator.isValidContainer($collapsePane);
                if (isValidCol) {
                    $anchor.removeClass('bv-col-error');
                }else{
                    $anchor.addClass('bv-col-error');
                }
                $icon.removeClass(faIcon.valid + " " + faIcon.invalid).addClass(isValidCol ? faIcon.valid : faIcon.invalid);
            }
        }
    });
});
