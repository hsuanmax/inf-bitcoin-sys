$(document).on('nifty.ready', function() {

    $('#demo-dp-range .input-daterange').datepicker({
        language:  'zh-TW',
        format: "yyyy/mm/dd",
        todayBtn: "linked",
        autoclose: true,
        todayHighlight: true
    });

    var faIcon = {
        valid: 'fa fa-check-circle fa-lg text-success',
        invalid: 'fa fa-times-circle fa-lg',
        validating: 'fa fa-refresh'
    }




    $('#addForm').bootstrapValidator({
        message: 'This value is not valid',
        excluded: ':disabled',
        feedbackIcons: faIcon,
        fields: {
            boughtBitcoin: {
                validators: {
                    notEmpty: {
                        message: '此欄位不得為空，最少為0'
                    },
                    numeric: {
                        message: '只能為數字'
                    }
                }
            },
            freeBitcoin: {
                validators: {
                    notEmpty: {
                        message: '此欄位不得為空，最少為0'
                    },
                    numeric: {
                        message: '只能為數字'
                    }
                }
            },
            startDate: {
                trigger:'change',
                validators: {
                    notEmpty: {
                        message: '此欄位不得為空'
                    }
                }
            },
            endDate: {
                trigger:'change',
                validators: {
                    notEmpty: {
                        message: '此欄位不得為空'
                    }
                }
            }
        }
    }).on('status.field.bv', function(e, data) {
        var $form = $(e.target),
            validator = data.bv,
            $collapsePane = data.element.parents('.collapse'),
            colId = $collapsePane.attr('id');

        if (colId) {
            var $anchor = $('a[href="#' + colId + '"][data-toggle="collapse"]');
            var $icon = $anchor.find('i');

            // Add custom class to panel containing the field
            if (data.status == validator.STATUS_INVALID) {
                $anchor.addClass('bv-col-error');
                $icon.removeClass(faIcon.valid).addClass(faIcon.invalid);
            } else if (data.status == validator.STATUS_VALID) {
                var isValidCol = validator.isValidContainer($collapsePane);
                if (isValidCol) {
                    $anchor.removeClass('bv-col-error');
                }else{
                    $anchor.addClass('bv-col-error');
                }
                $icon.removeClass(faIcon.valid + " " + faIcon.invalid).addClass(isValidCol ? faIcon.valid : faIcon.invalid);
            }
        }
    });

});

$(document).ready(function(){
    $("#boughtBitcoin").on('blur',function(){
        sum();
    });
    $("#freeBitcoin").on('blur',function(){
        sum();
    });
});

function sum(){
    var boughtValue = $("#boughtBitcoin").val()*1;
    var freeValue = $("#freeBitcoin").val()*1;
    $("#totalBitcoin").val(freeValue+boughtValue);
}