
// Tables-DataTables.js
// ====================================================================
// This file should not be included in your project.
// This is just a sample how to initialize plugins or components.
//
// - ThemeOn.net -



$(document).ready(function() {


    // DATA TABLES
    // =================================================================
    // Require Data Tables
    // -----------------------------------------------------------------
    // http://www.datatables.net/
    // =================================================================

    $.fn.DataTable.ext.pager.numbers_length = 5;
    

    // Add Row
    // -----------------------------------------------------------------
    var t = $('#demo-dt-addrow').DataTable({
        "responsive": true,
        "language": {
            "paginate": {
                "previous": '<i class="psi-arrow-left"></i>',
                "next": '<i class="psi-arrow-right"></i>'
            }
        },
        "dom": '<"newtoolbar">frtip',
        "columns": [
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            { "width": "15%" }
        ]
    });
    $('#demo-custom-toolbar2').appendTo($("div.newtoolbar"));





});

function deleteMember(mid){
    if(confirm("你確定要刪除 帳號:"+mid+" ?")){
        $("#deleteId").val(mid);
        $("#deleteForm").submit();
    }else{
        return ;
    }

}

function goToOrderPage(mid){
    $("#wonerId").val(mid);
    $("#goToOrderForm").submit();
}

function editMember(mid){
    $("#editId").val(mid);
    $("#memberEditForm").submit();
}