$(document).on('nifty.ready', function() {


    var faIcon = {
        valid: 'fa fa-check-circle fa-lg text-success',
        invalid: 'fa fa-times-circle fa-lg',
        validating: 'fa fa-refresh'
    }




    $('#addForm').bootstrapValidator({
        message: 'This value is not valid',
        excluded: ':disabled',
        feedbackIcons: faIcon,
        fields: {
            memberId:{
                validators: {
                    notEmpty: {
                        message: '此欄位不得為空'
                    },
                    regexp: {
                        regexp: /^[A-Z_0-9]+$/i,
                        message: '此欄位只可輸入英文、數字底線'
                    },
                    remote: {
                        message: '此ID已經存在',
                        method: 'GET',
                        url: 'checkMemberId',
                    }
                }
            },
            hierarchyList: {
                validators: {
                    callback: {
                        message: '請進行設定',
                        callback: function(value, validator) {
                            // Get the selected options
                            var options = validator.getFieldElements('hierarchyList').val();
                            return (options != null && options.length != 0);
                        }
                    }
                }
            },
            name: {
                validators: {
                    notEmpty: {
                        message: '此欄位不得為空'
                    },
                    regexp: {
                        regexp: /^[\u4e00-\u9fa5\s]+$/,
                        message: '此欄位只可輸入中文'
                    }
                }
            },
            englishName: {
                validators: {
                    notEmpty: {
                        message: '此欄位不得為空'
                    },
                    regexp: {
                        regexp: /^[A-Z\s]+$/i,
                        message: '此欄位只可輸入英文'
                    }
                }
            },
            referrerId: {
                validators: {
                    callback: {
                        message: '多體系領導人不可輸入推薦人代碼',
                        callback: function(value, validator) {
                            // Get the selected options
                            var options = validator.getFieldElements('hierarchyList').val();
                            if(options != null && options.length > 1 && $("#referrerId").val()){
                                return {
                                    valid: false,
                                    message: '多體系領導人不可輸入推薦人代碼'
                                };
                            }

                            if(options != null && options.length == 1 && $("#referrerId").val() == "" ){
                                return {
                                    valid: false,
                                    message: '此欄位不得為空'
                                };
                            }
                            return true;

                        }
                    },
                    regexp: {
                        regexp: /^[A-Z_0-9]+$/i,
                        message: '此欄位只可輸入英文、數字且不得有空格及任何符號'
                    },
                    remote: {
                        message: '無效的推薦人ID',
                        method: 'GET',
                        url: 'validReferrerId',
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: '此欄位不得為空'
                    },
                    emailAddress: {
                        message: '格式不符'
                    }
                }
            }
        }
    }).on('status.field.bv', function(e, data) {
        var $form = $(e.target),
            validator = data.bv,
            $collapsePane = data.element.parents('.collapse'),
            colId = $collapsePane.attr('id');

        if (colId) {
            var $anchor = $('a[href="#' + colId + '"][data-toggle="collapse"]');
            var $icon = $anchor.find('i');

            // Add custom class to panel containing the field
            if (data.status == validator.STATUS_INVALID) {
                $anchor.addClass('bv-col-error');
                $icon.removeClass(faIcon.valid).addClass(faIcon.invalid);
            } else if (data.status == validator.STATUS_VALID) {
                var isValidCol = validator.isValidContainer($collapsePane);
                if (isValidCol) {
                    $anchor.removeClass('bv-col-error');
                }else{
                    $anchor.addClass('bv-col-error');
                }
                $icon.removeClass(faIcon.valid + " " + faIcon.invalid).addClass(isValidCol ? faIcon.valid : faIcon.invalid);
            }
        }
    });

});


$(document).ready(function(){


    $("#hierarchyList").on("change",function(){
        var count = $("#hierarchyList :selected").length;
        if(count > 1) {
            var general = $("#general").prop("checked");
            if (general) {
                alert("一般會員不得設定多體系");
                $("#general").prop("checked", false)
            }
        }


    });

    $("#general").on("change",function(){
        var general = $("#general").prop("checked");
        var hierarchyListCount = $("#hierarchyList :selected").length;
        if( general && hierarchyListCount >1){
            alert("一般會員不得設定多體系");
            $("#general").prop("checked",false)
        }
    });

});