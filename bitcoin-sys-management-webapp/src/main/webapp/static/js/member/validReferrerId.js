$(document).ready(function(){
    $("#referrerId").blur(function(){
        var $groupDiv = $("#referrerId").parent().parent();
        var classValue = $groupDiv.attr("class");
        var hasSuccess = inputElementHasSuccess(classValue);
        if(hasSuccess){
            getMemberCategeryInfo();
            return;
        }
        var hasError = inputElementHasError(classValue);
        if(hasError) $("#memberCategory").val("");

    });

    function inputElementHasSuccess(classValue){
        var classes = classValue.split(" ");
        var result = false;
        $.each( classes, function( key, value ) {
            if(value == 'has-success') result = true;
        });
        return result;
    }
    function inputElementHasError(classValue){
        var classes = classValue.split(" ");
        var result = false;
        $.each( classes, function( key, value ) {
            if(value == 'has-error') result = true;
        });
        return result;
    }

    function getMemberCategeryInfo(){
        $.ajax({
            url: "getCategoryInfo",
            method: "GET",
            data: { referrerId : $("#referrerId").val() },
            dataType: "json"
        }).done(function( result ) {
            $("#memberCategory").val(result.name);
            $("#hierarchyId").val(result.id);
            console.log( 'id:'+result.id);
        }).fail(function( jqXHR, textStatus ) {
            alert( "Request failed: " + textStatus );
        });
    }
});