package com.inf.bitcoin.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/**
 * Created by max on 2018/9/14.
 */
public class BitcoinSysMemberToken extends UsernamePasswordAuthenticationToken {

    private String ipAddress;

    public BitcoinSysMemberToken(Object principal, Object credentials, String ipAddress) {
        super(principal, credentials);
        this.ipAddress = ipAddress;
    }

    public BitcoinSysMemberToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities, String memberName) {
        super(principal, credentials, authorities);
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }
}
