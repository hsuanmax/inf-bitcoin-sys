package com.inf.bitcoin.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/**
 * Created by max on 2018/9/14.
 */
public class BitcoinSysMemberDetails extends User {

    private String memberName;
    private boolean hasChangePwd;

    public BitcoinSysMemberDetails(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public boolean getHasChangePwd() {
        return hasChangePwd;
    }

    public void setHasChangePwd(boolean hasChangePwd) {
        this.hasChangePwd = hasChangePwd;
    }
}
