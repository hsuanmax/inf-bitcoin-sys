package com.inf.bitcoin.controller;

import com.inf.bitcoin.model.Order;
import com.inf.bitcoin.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping("adminOrder")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping
    public String list(Model model, @RequestParam String mid){
        model.addAttribute("orderListForOwner",orderService.orderListForOwner(mid));
        return "order/list";
    }

    @GetMapping("add")
    public String add(Model model){

        model.addAttribute("order",new Order());
        return "order/add";
    }

    @PostMapping(path="create",produces="text/plain;charset=UTF-8")
    public String create(@ModelAttribute Order order,@SessionAttribute String memberId,@RequestParam String mid){

        orderService.createOrder(order,mid,memberId);
        return "redirect:/adminOrder?mid="+mid;
    }

    @GetMapping("edit")
    public String edit(Model model,@RequestParam String orderId) {
        model.addAttribute("order",orderService.getOrderById(orderId));
        return "order/edit";
    }

    @PostMapping("store")
    public String store(@ModelAttribute Order order,@SessionAttribute String memberId,@RequestParam String mid)
    {
        orderService.modifyOrder(order,memberId);
        return "redirect:/adminOrder?mid="+mid;
    }

    @PostMapping("delete")
    public String delete(@RequestParam String mid,@RequestParam String orderId){
        orderService.deleteOrder(orderId);
        return "redirect:/adminOrder?mid="+mid;

    }
}
