package com.inf.bitcoin.controller;

import com.inf.bitcoin.bean.form.ReportIncomeForm;
import com.inf.bitcoin.service.HierarchyService;
import com.inf.bitcoin.service.ReportService;
import com.inf.bitcoin.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
//@RequestMapping("adminReport")
public class ReportController {
    @Autowired
    private ReportService reportService;

    @Autowired
    private HierarchyService hierarchyService;

    @Autowired
    private RoleService roleService;

    @RequestMapping(value="adminReportIncome", method = {RequestMethod.POST,RequestMethod.GET})
    public String dashboard(@ModelAttribute ReportIncomeForm reportIncomeForm, Model model){
//        session.setAttribute("memberId", "rick");
//        session.setAttribute("memberName", "Rick Lin");
//        session.setAttribute("isLeader", true);
        ;
        model.addAttribute("sysHierarchyList",hierarchyService.findAll());
        model.addAttribute("leaderList",roleService.getMemberListByLeader());
        model.addAttribute("reportList",reportService.getReportIncomeList(reportIncomeForm));
        model.addAttribute("reportIncomeForm",reportIncomeForm) ;
        return "report/incomeReport";
    }
}
