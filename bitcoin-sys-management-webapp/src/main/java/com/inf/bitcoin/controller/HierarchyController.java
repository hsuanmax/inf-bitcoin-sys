package com.inf.bitcoin.controller;

import com.inf.bitcoin.service.HierarchyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@Controller
//@RequestMapping("adminHierarchy")
public class HierarchyController {

    @Autowired
    private HierarchyService hierarchyService;

    @GetMapping("adminHierarchyList")
    public String list(Model model){
        model.addAttribute("hierarchyList",hierarchyService.findAll());
       return "hierarchy/list";
    }

    @GetMapping("adminHierarchyAdd")
    public String add(){
        return "hierarchy/add";
    }

    @PostMapping("adminHierarchyCreate")
    public String create(@RequestParam String name, @SessionAttribute String memberId) {
        hierarchyService.saveHierarchy(name, memberId);
        return "redirect:/adminHierarchyList";
    }

    @GetMapping("adminHierarchyEdit")
    public String edit(Model model, @RequestParam String hid) {
        model.addAttribute("hierarchy",hierarchyService.findById(hid));
        return "hierarchy/edit";
    }

    @PostMapping("adminHierarchyStore")
    public String store(@RequestParam String hid,@RequestParam String name, @SessionAttribute String memberId){
        hierarchyService.modifyHierarchy(hid,name,memberId);
        return "redirect:/adminHierarchyList";
    }

    @PostMapping("adminHierarchyDelete")
    public String delete(@RequestParam String hid){
        hierarchyService.deleteHierarchy(hid);
        return "redirect:/adminHierarchyList";
    }

    @GetMapping("adminHierarchyCheckName")
    @ResponseBody
    public Map<String,Boolean> checkName(@RequestParam String name){
        Map<String,Boolean> result = new HashMap<>();
        result.put("valid",hierarchyService.hasNotSameName(name));
        return result;
    }
}
