package com.inf.bitcoin.controller;

import com.inf.bitcoin.service.SystemConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("adminSystemConfig")
public class SystemConfigController {

    @Autowired
    private SystemConfigService systemConfigService;

    @GetMapping
    public String getSystemConfigList(Model model){
        model.addAttribute("systemConfigList",systemConfigService.findAll());
        return "systemConfig/info";
    }

    @PostMapping("store")
    public String modifySystemConfig(@RequestParam String configId,@RequestParam String configValue){
        systemConfigService.modifySystemConfig(configId,configValue);
        return "redirect:/adminSystemConfig";
    }
}
