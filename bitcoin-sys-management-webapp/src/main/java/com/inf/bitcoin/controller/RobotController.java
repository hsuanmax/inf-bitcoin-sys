package com.inf.bitcoin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class RobotController {
    @RequestMapping(value = {"/robots", "/robot", "/robot.txt", "/robots.txt", "/null"})
    public void robot( HttpServletResponse response) {

        try {
           response.setContentType("text/plain");
           response.getWriter().write("User-agent: *\nDisallow: / \n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
