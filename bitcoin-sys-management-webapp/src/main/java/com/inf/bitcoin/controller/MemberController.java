package com.inf.bitcoin.controller;

import com.inf.bitcoin.bean.form.AdminMemberBean;
import com.inf.bitcoin.model.Hierarchy;
import com.inf.bitcoin.model.Member;
import com.inf.bitcoin.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
//@RequestMapping("adminMember")
public class MemberController {

    @Autowired
    private MemberService memberService;

    @GetMapping("adminMemberList")
    public String list(Model model){
        model.addAttribute("memberList",memberService.getAdminMemberList());
        return "member/list";
    }

    @GetMapping("adminMemberAdd")
    public String add(Model model){

        model.addAttribute("sysHierarchyList",memberService.getHierarchyList());
        return "member/add";
    }

    @PostMapping("adminMemberCreate")
    public String create(@ModelAttribute AdminMemberBean formBean ,@SessionAttribute String memberId){
        memberService.createMemberInfo(formBean,memberId);
        return "redirect:/adminMember";
    }

    @GetMapping("adminMemberEdit")
    public String edit(Model model, @RequestParam String mid) {
        Member member = memberService.getMemberById(mid);
        model.addAttribute("member", member);

        List<Long> hierarchyIdList = memberService.getMemberHierarchyIdList(member);

        List<String> roleList = memberService.getMemberRoleIdList(member);

        model.addAttribute("sysHierarchyList",memberService.getHierarchyList());
        model.addAttribute("memberHierarchyIdList",hierarchyIdList);

        model.addAttribute("memberRoleIdList",roleList);

        return "member/edit";
    }

    @PostMapping("adminMemberStore")
    public String store(@ModelAttribute AdminMemberBean formBean,@SessionAttribute String memberId){
        memberService.modifyMemberInfo(formBean,memberId);
        return "redirect:/adminMember";
    }

    @PostMapping("adminMemberDelete")
    public String delete(@RequestParam String mid){
        memberService.delMember(mid);
        return "redirect:/adminMember";
    }



    /**
     * 檢查推薦人是否存在
     * @param referrerId
     * @return
     */
    @RequestMapping("validReferrerId")
    @ResponseBody
    public Map<String,Object> referrerIdValid(@RequestParam(required=false) String referrerId){
        Map<String,Object> result = new HashMap<>();
        if(StringUtils.isEmpty(referrerId)) {
            result.put("valid",true);
            return result;
        }
        // 用js的套件驗證，所以只能回傳valid是true/false; by Rick
        Member member = memberService.getMemberById(referrerId);
        result.put("valid",false);
        if(member != null) {
            result.put("valid",true);
        }

        return result;
    }


    /**
     * 檢查ID是否已經使用
     * @param memberId
     * @return
     */
    @RequestMapping("checkMemberId")
    @ResponseBody
    public Map<String,Object> checkMemberId(@RequestParam String memberId){
        Map<String,Object> result = new HashMap<>();
        // 用js的套件驗證，所以只能回傳valid是true/false; by Rick
        Member member = memberService.getMemberById(memberId);
        result.put("valid",true);
        if(member != null) {
            result.put("valid",false);
        }

        return result;
    }

    /**
     * 查詢推薦人的體系
     * @param referrerId
     * @return
     */
    @RequestMapping("getCategoryInfo")
    @ResponseBody
    public Map<String,Object> getMemeberInfo(@RequestParam String referrerId){
        Map<String,Object> result = new HashMap<>();
        result.put("name","");
        result.put("id","");
        Member member = memberService.getMemberById(referrerId);
        if(member != null){
            // js會發ajax上來，目前js是取name跟id
            // 如有其他需求 請改memberAdd.js的 getMemberCategeryInfo()內容

            List<Hierarchy> hierarchyList = memberService.getMemeberHierarchy(referrerId);

            if(CollectionUtils.isEmpty(hierarchyList) || hierarchyList.size()>1) return result;
            Hierarchy hierarchy = hierarchyList.get(0);
            result.put("name",hierarchy.getName());
            result.put("id",hierarchy.getHid());
        }
        return result;
    }
}
