package com.inf.bitcoin.controller;

import com.inf.bitcoin.model.Category;
import com.inf.bitcoin.model.News;
import com.inf.bitcoin.service.CategoryService;
import com.inf.bitcoin.service.NewsService;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Controller
public class NewsController {

    @Autowired
    private NewsService newsService;

    @Autowired
    private CategoryService categoryService;

    @GetMapping("adminNewsList")
    public String adminNewsList(Model model){
        List<News> newsList = newsService.listNews();
        model.addAttribute("newsList", newsList);

        return "news/list";
    }

    @GetMapping("adminNewsAdd")
    public String adminNewsAdd(Model model){
        News news = new News();
        news.setCategory(new Category());
        List<Category> categoryList = categoryService.listCategory();
        model.addAttribute("news", news);
        model.addAttribute("categoryList", categoryList);

        return "news/add";
    }

    @PostMapping("adminNewsAddSubmit")
    public String adminAddNewsSubmit(@ModelAttribute News news, BindingResult result,
                                     Model model, @SessionAttribute String memberId) {
        if(news.getFile().isEmpty()) {
            List<Category> categoryList = categoryService.listCategory();
            model.addAttribute("news", news);
            model.addAttribute("categoryList", categoryList);
            model.addAttribute("errorMsg", "請選擇圖檔上傳");

            return "news/add";
        }
        newsService.uploadImgFile(news);
        Category category = new Category();
        category.setId(Long.valueOf(news.getCategoryId()));
        news.setCategory(category);
        news.setCreateDate(new Date());
        news.setCreateId(memberId);
        newsService.saveNews(news);

        return "redirect:/adminNewsList";
    }

    @GetMapping("adminNewsEdit")
    public String adminNewsEdit(Model model, @RequestParam Long id) throws ParseException{
        News news = newsService.getNewsForEditoOrShow(id);
        List<Category> categoryList = categoryService.listCategory();
        news.setCategoryId(news.getCategory().getId().toString());

        model.addAttribute("news", news);
        model.addAttribute("categoryList", categoryList);

        return "news/edit";
    }

    @PostMapping("adminNewsEditSubmit")
    public String adminNewsEditSubmit(@ModelAttribute News news, BindingResult result) {
        if(!news.getFile().isEmpty()) {
            newsService.uploadImgFile(news);
        }else {
            news.setImageName(news.getImageName().split("/")[1]);
        }
        Category category = new Category();
        category.setId(Long.valueOf(news.getCategoryId()));
        news.setCategory(category);
        newsService.saveNews(news);

        return "redirect:/adminNewsList";
    }

    @PostMapping("adminNewsDelSubmit")
    public String adminNewsDelSubmit(@RequestParam Long id){
        newsService.delNews(id);

        return "redirect:/adminNewsList";
    }

    @GetMapping(value = "image/{imageName:.+}")
    @ResponseBody
    public byte[] getImage(@PathVariable String imageName) throws IOException {

        return newsService.getImage(imageName);
    }
}
