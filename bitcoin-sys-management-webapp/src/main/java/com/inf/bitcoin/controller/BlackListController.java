package com.inf.bitcoin.controller;

import com.inf.bitcoin.model.BlackList;
import com.inf.bitcoin.service.BlackListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.TimeZone;

/**
 * Created by max on 2018/10/10.
 */
@Controller
public class BlackListController {

    @Autowired
    private BlackListService blackListService;

    @GetMapping("adminBlackList")
    public String list(Model model) {
        List<BlackList> blackList = blackListService.listAll();
        model.addAttribute("blackList", blackList);

        return "blackList/list";
    }

    @PostMapping("adminUnlock")
    public String unlock(@RequestParam List<Long> unlock) {
        for(Long id:unlock) {
            blackListService.unLock(id);
        }

        return "redirect:/adminBlackList";
    }
}
