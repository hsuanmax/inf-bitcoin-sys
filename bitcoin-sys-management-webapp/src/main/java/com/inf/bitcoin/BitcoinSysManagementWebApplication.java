package com.inf.bitcoin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.context.request.RequestContextListener;

/**
 * Created by max on 2017/8/30.
 */
@SpringBootApplication
public class BitcoinSysManagementWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(BitcoinSysManagementWebApplication.class, args);
    }

    @Bean
    public RequestContextListener requestContextListener() {
        return new RequestContextListener();
    }
}
