package com.inf.bitcoin.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Created by max on 2017/9/6.
 */
@Configuration
@Import(CoreConfig.class)
public class WebRootConfig {
}
