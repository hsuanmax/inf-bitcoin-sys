package com.inf.bitcoin.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.Filter;
import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletRegistration;

/**
 * Created by max on 2017/8/25.
 */
public class WebInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Value("${upload.image.path}")
    private String uploadImgPath;

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{WebRootConfig.class, SecurityConfig.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{WebConfig.class};
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

    @Override
    protected Filter[] getServletFilters() {
        CharacterEncodingFilter encodingFilter = new CharacterEncodingFilter();
        encodingFilter.setForceEncoding(true);
        encodingFilter.setForceRequestEncoding(true);
        encodingFilter.setEncoding("UTF-8");

        return new Filter[]{encodingFilter};

    }

    @Override
    protected void customizeRegistration(ServletRegistration.Dynamic registration) {
        MultipartConfigElement multipartConfigElement = new MultipartConfigElement(uploadImgPath, 5048576,
                50485760, 0);
        registration.setMultipartConfig(multipartConfigElement);
    }
}