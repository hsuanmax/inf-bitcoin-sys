package com.inf.bitcoin.config;

import com.inf.bitcoin.security.BitcoinSysAuthenticationProvider;
import com.inf.bitcoin.security.BitcoinSysLoginFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.session.HttpSessionEventPublisher;

/**
 * Created by max on 2018/9/13.
 */

@Configuration
@EnableWebSecurity
@ComponentScan("com.inf.bitcoin.security")
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private BitcoinSysAuthenticationProvider bitcoinSysAuthenticationProvider;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
            .antMatchers("/admin**","/admin**/**").authenticated()
            .and()
            .addFilterBefore(bitcoinSysLoginFilter(), UsernamePasswordAuthenticationFilter.class)
            .formLogin()
            .loginPage("/login")
            .and()
            .logout().logoutUrl("/logout")
            .logoutSuccessUrl("/login ");

        http.sessionManagement()
            .invalidSessionUrl("/login")
            .maximumSessions(1)
            .maxSessionsPreventsLogin(true)
            .expiredUrl("/login?expired")
            .sessionRegistry(sessionRegistry());

    }

    @Bean
    public SessionRegistry sessionRegistry() {
        SessionRegistry sessionRegistry = new SessionRegistryImpl();
        return sessionRegistry;
    }

    @Bean
    public static ServletListenerRegistrationBean httpSessionEventPublisher() {
        return new ServletListenerRegistrationBean(new HttpSessionEventPublisher());
    }

    @Bean
    public BitcoinSysLoginFilter bitcoinSysLoginFilter()
            throws Exception {
        BitcoinSysLoginFilter bitcoinSysLoginFilter =
                new BitcoinSysLoginFilter("/login", authenticationManager());
        bitcoinSysLoginFilter.setAuthenticationManager(authenticationManagerBean());

        return bitcoinSysLoginFilter;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(bitcoinSysAuthenticationProvider);
    }

}
