package com.inf.bitcoin.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by max on 2018/2/26.
 */
@ControllerAdvice
public class BitcoinSysExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(BitcoinSysExceptionHandler.class);

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(Exception.class)
    public void internalServerError(Exception ex){
        ex.printStackTrace();
        logger.info(ex.getCause().getMessage());
    }

}
